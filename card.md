**Register card**
----
  Register card

* **URL**

  /api/v1/cards

* **Method:**
  
  `POST`
  
*  **URL Params**

   None 

* **Data Params**

  **Required:**
   
    `phone_number=[string]`
    
    `plan_id=[string]`
    
    `payment_method=[string]`

* **Success Response:**
  

  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id" : "string",
        "card_number": "string",
        "expired_at": "datetime",
        "status": "string" 
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

  * **Status Code:** 400 Bad Request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request was unacceptable"
      } 
    }
    ```

* **Sample Call:**

```bash
curl -X POST \
  http://localhost:8086/v1/cards \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDU4NDIsImlhdCI6MTU1MjY0NDk0MiwiUGhvbmVOdW1iZXIiOiIiLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfQURNSU4ifQ.zHOGERD4lsH3YOjacQbFEhGgkRs7SSYoXqxDtfyWqq4' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 322ab502-6554-58b7-a550-9ec549102b91' \
  -d '{
	"name":"Card 1",
	"plan_id": 1
}'
```

* **Notes:**


**Renew card**
----
  Renew card

* **URL**

  /api/v1/cards/renew

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
    `plan_id=[string]`
    
    `payment_method=[string]`


* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id" : "string",
        "card_number": "string",
        "status": "string" 
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
  
  * **Status Code:** 400 Bad Request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request was unacceptable"
      } 
    }
    ```
      
      
* **Notes:**

  

**Retrieve the latest card**
----
  Retrieve the latest card

* **URL**

  /api/v1/cards/latest

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": 1,
        "plan": {
          "id": 1,
          "name": "plan2",
          "description": "description plan 2",
          "price": 800000,
          "duration": 8,
          "order": 0,
          "status": 1,
          "create_at": "2019-03-15T17:15:56.561254+07:00",
          "update_at": "2019-03-15T17:15:56.561254+07:00"
        },
        "name": "Card 1",
        "activation_code": "YZRK1-GQD8Q-ZY2Z5-JOG0Z",
        "payment_method": "",
        "expired_at": "2019-11-15T17:16:12.132389773+07:00",
        "create_at": "2019-03-15T10:16:12.133197701Z",
        "update_at": "2019-03-15T10:16:12.134578082Z",
        "status": 1
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
      
* **Notes:**

  
  