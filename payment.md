**List bank**
----

* **URL**

  /api/v1/banks

* **Method:**

   `GET`

*  **URL Params**

   **Optional:**

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop

* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "name": "string",
          "full_name": "string",
          "account_name": "string",
          "account_id": "string",
          "image_url": "string",
          "branch_name": "string",
          "message": "string"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

* **Notes:**



**Payment history**
----

  Returns a list payments of user

* **URL**

  /api/v1/payments

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**

    `user_id=[string]`

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop


* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "content": "string",
          "amount": "float",
          "status": "bool",
          "created_at": "Datetime"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
  
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**


**Payment detail**
----
  Retrieves the details of an payment.

* **URL**

  /api/v1/payment/{payment_id}

* **Method:**

  `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "content": "string",
        "amount": "float",
        "bank_name": "string",
        "bank_account_name": "string",
        "bank_account_number": "string",
        "owner_name": "string",
        "status": "bool",
        "created_at": "Datetime"
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**



**Wallet info**
----
  Retrieves the details wallet of user.

* **URL**

  /api/v1/wallet/{user_id}

* **Method:**

  `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "user_id": "string",
        "balance": "int",
        "status": "bool",
        "created_at": "Datetime",
        "updated_at": "Datetime"
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**



**Wallet transaction**
----

  Returns a list transactions of user


* **URL**

  /api/v1/transactions

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `wallet_id=[string]`

   **Optional:**

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop


* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "amount": "float",
          "type": "string",
          "description": "string",
          "status": "bool",
          "created_at": "Datetime"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**












