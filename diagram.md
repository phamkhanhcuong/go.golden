# Diagram

## login
```mermaid
sequenceDiagram

User->>+Account Kit: phone number


Account Kit-->>-User: Auth code

User->>+Backend: call login api with auth code & device token

Backend-->>-User: Access token

```


## Booking status
```
Reject = 0

// not required to pre-charge
Created = 1

// required pre-charge. merchant dont see the booking in this status
Payment = 2

// used. set by merchant
Done = 3

// Paid. set by seller for the bookings are required to pre-charge
Paid = 4

//
Golden = 5

//
Accepted = 6
```

```mermaid
graph LR
    Booking -- not required pre-charge --> Created
    Booking -- required pre-charge --> Payment
    Booking -- not required pre-charge but pre-charge --> Golden
    Payment --> Paid
    Golden --> Paid
    Created --> Accepted
    Accepted --> Done
    Paid --> Done
    Created --> Reject
    Payment --> Reject
    Golden --> Reject
```