
**Voucher create**
----

* **URL**

   /api/v1/vouchers

* **Method:**

   `POST`

* **URL Params**

   None

* **Data Params**

   **Required:**

   `deal_id=[string]`


* **Success Response:**

    * **Code:** 200 <br />
      **Content:**
      ```json
      {
        "data": [
          {
            "id": "string",
            "deal": "Deal",
            "code": "string",
            "discount": "int",
            "expired_at": "Datetime",
            "created_at": "Datetime"
          }
        ],
        "error": { "code": 200 }
      }
      ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
      
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**



**Voucher check**
----

* **URL**

   /api/v1/voucher_check/{code}

* **Method:**

   `GET`

* **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data": [
          {
            "id": "string",
            "deal": "Deal",
            "code": "string",
            "discount": "int",
            "expired_at": "Datetime",
            "created_at": "Datetime"
          }
        ],
        "error": { "code": 200 }
      }
    ```

* **Error Response:**

    * **Status Code:** 400 Bad request <br />
      **Content:**
      ```json
      {
        "error" : {
          "code": 400,
          "message": "The request is invalid"
        }
      }
      ```

  * **Status Code:** 401 Unauthorized <br />
      **Content:**
      ```json
      {
        "error" : {
          "code": 401,
          "message": "Authentication is required"
        }
      }
      ```

* **Notes:**



**Voucher list**
----

* **URL**

   /api/v1/vouchers

* **Method:**

   `GET`

*  **URL Params**

   **Optional:**

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop


* **Data Params**

    None

* **Success Response:**

   * **Code:** 200 <br />
      **Content:**
      ```json
      {
        "data": [
          {
            "id": "string",
            "deal": "Deal",
            "status": "int",
            "is_expried": "bool",
            "used_at": "Datetime",
            "expired_at": "Datetime"
          },
          { ... }
        ],
        "metadata": {
          "total": 2,
          "result": 2,
          "page": 1,
          "limit": 10
        },
        "error": { "code": 200 }
      }
      ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
      
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**



**Delete a voucher**
----
  Delete a voucher

* **URL**

  /api/v1/vouchers/{voucher_id}

* **Method:**

  `DELETE`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 404 Not found  <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```


* **Notes:**