**** == Transaction For Admin == ****
--------
**Transaction Deposit**
----

  Create Transaction Deposit

* **URL**

  /api/v1/admin/transactions/admin_deposit

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`
  
*  **URL Params**

   None

* **Data Params**
    
    `user_id=[string]`
    `amount=[float]`
    `note=[string]`


* **Success Response:**
  
  * **Code:** 201 <br />
    **Content:** 
    ```json
     {
       "error": {
         "code": 201,
         "message": "Đã tạo"
       },
       "data": {
         transaction
       }
     }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
 
  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```


* **Sample Call:**
  ```bash
  curl -X POST \
    http://localhost:8080/v1/admin/transactions/admin_deposit \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    	"user_id":"4dxK0EyJn8wO9QrYP8pv",
    	"amount":10000,
    	"note":"test"
        }'
  ```
  Response:
  ```json
  {
    "error": {
      "code": 201,
      "message": "Đã tạo"
    },
    "data": {
      "id": 1,
      "code": "GDQ3R021NOYJK984YP6M",
      "type": "AdminDeposit",
      "amount": 10000,
      "amount_change": "+",
      "balance_before": 0,
      "balance_after": 10000,
      "name": "Admin cộng điểm",
      "description": "",
      "note": "test note",
      "user": {
        "id": 272,
        "user_id": "4dxK0EyJn8wO9QrYP8pv",
        "balance": 10000,
        "fingerprint": "",
        "phone_number": "+84788448730",
        "_": "",
        "card_number": "",
        "first_name": "Pham Khanh Cuong",
        "last_name": "",
        "dob": "1985/01/12",
        "gender": "male",
        "avatar": "",
        "address": "HP",
        "cmnd": "",
        "email": "phamkhanhcuong@gmail.com",
        "note": "test",
        "role": "ROLE_USER",
        "created_at": "2019-07-04T10:18:14.56849Z",
        "updated_at": "2019-07-27T02:44:45.428178Z",
        "status": 1,
        "expired_at": "0001-01-01T00:00:00Z"
      },
      "status": 1,
      "created_at": "2019-07-27T02:44:45.346262Z",
      "updated_at": "2019-07-27T02:44:45.455104Z",
      "data": null
    }
  }
  ```

* **Notes:**


**Transaction Withdraw**
----

  Create Transaction Withdraw

* **URL**

  /api/v1/admin/transactions/admin_withdraw

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    `user_id=[string]`
    `amount=[float]`
    `note=[string]`


* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
    {
       "error": {
         "code": 201,
         "message": "Đã tạo"
       },
       "data": {
         transaction
       }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```


* **Sample Call:**
  ```bash
  curl -X POST \
    http://localhost:8080/v1/admin/transactions/admin_withdraw \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    	"user_id":"4dxK0EyJn8wO9QrYP8pv",
    	"amount":10000,
    	"note":"test"
        }'
  ```
  Response:
  ```json
  {
    "error": {
      "code": 201,
      "message": "Đã tạo"
    },
    "data": {
      "id": 4,
      "code": "1W30KGMEJVONQ7Y5XRD4",
      "type": "AdminWithdraw",
      "amount": 10000,
      "amount_change": "-",
      "balance_before": 10000,
      "balance_after": 0,
      "name": "Admin trừ điểm",
      "description": "",
      "note": "test",
      "user": {
        "id": 272,
        "user_id": "4dxK0EyJn8wO9QrYP8pv",
        "balance": 0,
        "fingerprint": "",
        "phone_number": "+84788448730",
        "_": "",
        "card_number": "",
        "first_name": "Pham Khanh Cuong",
        "last_name": "",
        "dob": "1985/01/12",
        "gender": "male",
        "avatar": "",
        "address": "HP",
        "cmnd": "",
        "email": "phamkhanhcuong@gmail.com",
        "note": "ghi chu",
        "role": "ROLE_USER",
        "created_at": "2019-07-04T10:18:14.56849Z",
        "updated_at": "2019-07-27T03:04:44.018172Z",
        "status": 1,
        "expired_at": "0001-01-01T00:00:00Z"
      },
      "status": 1,
      "created_at": "2019-07-27T03:04:44.005007Z",
      "updated_at": "2019-07-27T03:04:44.036906Z",
      "data": null
    }
  }
  ```

* **Notes:**





**Transaction detail**
----

* **URL**

   /api/v1/admin/transactions/:id

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data":{
        transaction
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```


* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/admin/transactions/7 \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": {
        "id": 7,
        "code": "40Q9GY5OJM9LK317E8DV",
        "type": "OrderDeal",
        "amount": 200,
        "amount_change": "-",
        "balance_before": 9999.99,
        "balance_after": 9799.99,
        "name": "Mua Deal",
        "description": "Silk Village Resort &Spa",
        "note": "",
        "user": {
          "id": 272,
          "user_id": "4dxK0EyJn8wO9QrYP8pv",
          "balance": 9799.99,
          "fingerprint": "",
          "phone_number": "+84788448730",
          "_": "",
          "card_number": "",
          "first_name": "Pham Khanh Cuong",
          "last_name": "",
          "dob": "1985/01/12",
          "gender": "male",
          "avatar": "",
          "address": "HP",
          "cmnd": "",
          "email": "phamkhanhcuong@gmail.com",
          "note": "ghi chu",
          "role": "ROLE_USER",
          "created_at": "2019-07-04T10:18:14.56849Z",
          "updated_at": "2019-07-27T03:14:03.262059Z",
          "status": 1,
          "expired_at": "0001-01-01T00:00:00Z"
        },
        "status": 1,
        "created_at": "2019-07-27T03:14:03.257008Z",
        "updated_at": "2019-07-27T03:14:03.268322Z",
        "data": {
          "id": 262,
          "name": "Silk Village Resort &Spa",
          "sale_discount": 0,
          "sale_discount_type": "p",
          "sale_price": 200000,
          "type": "hotel"
        }
      }
    }
    ```

* **Notes:**




**Transaction list**
----

* **URL**

   /api/v1/admin/transactions

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**


* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        transaction,
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/admin/transactions \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": [
        {
          "id": 6,
          "code": "0WPK8OG6L1QJ1347E95D",
          "type": "OrderDeal",
          "amount": 0.01,
          "amount_change": "-",
          "balance_before": 10000,
          "balance_after": 9999.99,
          "name": "Mua Deal",
          "description": "Silk Village Resort &Spa",
          "note": "",
          "user": {
            "id": 272,
            "user_id": "4dxK0EyJn8wO9QrYP8pv",
            "balance": 9799.99,
            "fingerprint": "",
            "phone_number": "+84788448730",
            "_": "",
            "card_number": "",
            "first_name": "Pham Khanh Cuong",
            "last_name": "",
            "dob": "1985/01/12",
            "gender": "male",
            "avatar": "",
            "address": "HP",
            "cmnd": "",
            "email": "phamkhanhcuong@gmail.com",
            "note": "ghi chu",
            "role": "ROLE_USER",
            "created_at": "2019-07-04T10:18:14.56849Z",
            "updated_at": "2019-07-27T03:14:03.262059Z",
            "status": 1,
            "expired_at": "0001-01-01T00:00:00Z"
          },
          "status": 1,
          "created_at": "2019-07-27T03:12:36.332844Z",
          "updated_at": "2019-07-27T03:12:36.344247Z",
          "data": {
            "id": 262,
            "name": "Silk Village Resort &Spa",
            "sale_discount": 0,
            "sale_discount_type": "p",
            "sale_price": 10,
            "type": "hotel"
          }
        }
      ],
      "metadata": {
        "total": 1,
        "result": 1,
        "page": 1,
        "limit": 10
      }
    }
    ```

* **Notes:**

**** == Transaction For User == ****

--------

**Transaction buy deal**
----
  Transaction buy deal

* **URL**

  /api/v1/transactions/order_deal

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    `deal_id=[string]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
    {
      "data":{
        "deal_code": "string",
        "transaction": "transaction",
      },
      "error": { "code": 201 }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/transactions/order_deal \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -d '{
      	"deal_id":"4dxK0EyJn8wO9QrYP8pv"
    }'
    ```
    Response:

    ```json
    {
      "error": {
        "code": 201,
        "message": "Đã tạo"
      },
      "data": {
        "deal_code": "test10002",
        "transaction": {
          "id": 7,
          "code": "40Q9GY5OJM9LK317E8DV",
          "type": "OrderDeal",
          "amount": 200,
          "amount_change": "-",
          "balance_before": 9999.99,
          "balance_after": 9799.99,
          "name": "Mua Deal",
          "description": "Silk Village Resort &Spa",
          "note": "",
          "user": {
            "id": 272,
            "user_id": "4dxK0EyJn8wO9QrYP8pv",
            "balance": 9799.99,
            "fingerprint": "",
            "phone_number": "+84788448730",
            "_": "",
            "card_number": "",
            "first_name": "Pham Khanh Cuong",
            "last_name": "",
            "dob": "1985/01/12",
            "gender": "male",
            "avatar": "",
            "address": "HP",
            "cmnd": "",
            "email": "phamkhanhcuong@gmail.com",
            "note": "ghi chu",
            "role": "ROLE_USER",
            "created_at": "2019-07-04T10:18:14.56849Z",
            "updated_at": "2019-07-27T03:14:03.262059Z",
            "status": 1,
            "expired_at": "0001-01-01T00:00:00Z"
          },
          "status": 1,
          "created_at": "2019-07-27T03:14:03.257008Z",
          "updated_at": "2019-07-27T03:14:03.268322Z",
          "data": {
            "id": 262,
            "name": "Silk Village Resort &Spa",
            "sale_discount": 0,
            "sale_discount_type": "p",
            "sale_price": 200000,
            "type": "hotel"
          }
        }
      }
    }
    ```

* **Notes:**





**Transaction detail**
----

* **URL**

   /api/v1/transactions/code/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data":{
        transaction
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```


* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/transactions/code/40Q9GY5OJM9LK317E8DV \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": {
        "id": 7,
        "code": "40Q9GY5OJM9LK317E8DV",
        "type": "OrderDeal",
        "amount": 200,
        "amount_change": "-",
        "balance_before": 9999.99,
        "balance_after": 9799.99,
        "name": "Mua Deal",
        "description": "Silk Village Resort &Spa",
        "note": "",
        "user": {
          "id": 272,
          "user_id": "4dxK0EyJn8wO9QrYP8pv",
          "balance": 9799.99,
          "fingerprint": "",
          "phone_number": "+84788448730",
          "_": "",
          "card_number": "",
          "first_name": "Pham Khanh Cuong",
          "last_name": "",
          "dob": "1985/01/12",
          "gender": "male",
          "avatar": "",
          "address": "HP",
          "cmnd": "",
          "email": "phamkhanhcuong@gmail.com",
          "note": "ghi chu",
          "role": "ROLE_USER",
          "created_at": "2019-07-04T10:18:14.56849Z",
          "updated_at": "2019-07-27T03:14:03.262059Z",
          "status": 1,
          "expired_at": "0001-01-01T00:00:00Z"
        },
        "status": 1,
        "created_at": "2019-07-27T03:14:03.257008Z",
        "updated_at": "2019-07-27T03:14:03.268322Z",
        "data": {
          "id": 262,
          "name": "Silk Village Resort &Spa",
          "sale_discount": 0,
          "sale_discount_type": "p",
          "sale_price": 200000,
          "type": "hotel"
        }
      }
    }
    ```

* **Notes:**

    


**Transaction list**
----

* **URL**

   /api/v1/transactions

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**


* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        transaction,
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/transactions \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": [
        {
          "id": 6,
          "code": "0WPK8OG6L1QJ1347E95D",
          "type": "OrderDeal",
          "amount": 0.01,
          "amount_change": "-",
          "balance_before": 10000,
          "balance_after": 9999.99,
          "name": "Mua Deal",
          "description": "Silk Village Resort &Spa",
          "note": "",
          "user": {
            "id": 272,
            "user_id": "4dxK0EyJn8wO9QrYP8pv",
            "balance": 9799.99,
            "fingerprint": "",
            "phone_number": "+84788448730",
            "_": "",
            "card_number": "",
            "first_name": "Pham Khanh Cuong",
            "last_name": "",
            "dob": "1985/01/12",
            "gender": "male",
            "avatar": "",
            "address": "HP",
            "cmnd": "",
            "email": "phamkhanhcuong@gmail.com",
            "note": "ghi chu",
            "role": "ROLE_USER",
            "created_at": "2019-07-04T10:18:14.56849Z",
            "updated_at": "2019-07-27T03:14:03.262059Z",
            "status": 1,
            "expired_at": "0001-01-01T00:00:00Z"
          },
          "status": 1,
          "created_at": "2019-07-27T03:12:36.332844Z",
          "updated_at": "2019-07-27T03:12:36.344247Z",
          "data": {
            "id": 262,
            "name": "Silk Village Resort &Spa",
            "sale_discount": 0,
            "sale_discount_type": "p",
            "sale_price": 10,
            "type": "hotel"
          }
        }
      ],
      "metadata": {
        "total": 1,
        "result": 1,
        "page": 1,
        "limit": 10
      }
    }
    ```

* **Notes:**


