**Create a merchant**
----
  Create a merchant

* **URL**

  /api/v1/admin/merchants

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**


* **Data Params**

  **Required:**
  
    `category_id=[integer]`
    
    `city_id=[string]`
    
    `name=[string]`
    
  **Optional:**

    `description=[string]`
    
    `image[]=[string]`
    
    `primary_image=[integer]`     The index of primary image. The default is 0

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "owner_id": "string",
        "category_id": "string",
        "city_id": "string",
        "name": "string",
        "description": "string",
        "created_at": "string",
        "updated_at": "string",
        "status": "string"
      },
      "error": { "code": 200 } 
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
* **Notes:**


**List all merchants**
----
  Returns a list of merchants

* **URL**

  /api/v1/merchants

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop

* **Data Params**

  None

* **Success Response:**

 * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "owner_id": "string",
          "category_id": "string",
          "city_id": "string",
          "name": "string",
          "description": "string",
          "created_at": "string",
          "updated_at": "string",
          "status": "string",
          "images": "array"
        },
        {...}
      ],
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 404 Not found  <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```


* **Notes:**


**Retrieve Merchant**
----
  Retrieve a merchant

* **URL**

  /api/v1/merchants/{merchant_id}

* **Method:**
  
  `GET`
  
*  **URL Params**

   None

* **Data Params**

    None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "owner_id": "string",
        "category_id": "string",
        "city_id": "string",
        "name": "string",
        "description": "string",
        "created_at": "string",
        "updated_at": "string",
        "status": "string",
        "images": "array"
      },
      "error": { "code": 200 } 
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
    
* **Notes:**


**Update a merchant**
----
  Update a merchant

* **URL**

  /api/v1/admin/merchants/{merchant_id}

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Optional:**
        
  `category_id=[integer]`
  
  `city_id=[string]`
  
  `name=[string]`
  
  `description=[string]`
  
  `image[]=[string]`
  
  `primary_image=[integer]`     The index of primary image. The default is 0


* **Success Response:**
  
 * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "owner_id": "string",
        "category_id": "string",
        "city_id": "string",
        "name": "string",
        "description": "string",
        "created_at": "string",
        "updated_at": "string",
        "status": "string",
        "images": "array"
      },
      "error": { "code": 200 } 
    }
    ```
    
* **Error Response:**
  
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
* **Notes:**





**Delete a merchant**
----
  Delete a merchant

* **URL**

  /api/v1/admin/merchants/{merchant_id}

* **Method:**

  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
    
* **Notes:**



**Rating a merchant**
----
  Rating a merchant

* **URL**

  /api/v1/merchants/{merchant_id}/rate

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
   `rating=[integer]`


* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": { 
        "merchant_id": "string",
        "rating": "integer"
      },
      "error": { "code": 200 } 
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
   
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
* **Notes:**

  
   