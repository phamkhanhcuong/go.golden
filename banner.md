**Create a banner**
----
  Create a banner

* **URL**

  /api/v1/admin/banner

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
   `name=[string]`
   
   `position=[string]`
   
   `description=[string]`
   
   `order=[number]`
   
   `status=[number]`
   
   `items=[Array.<Items>]`    Array of object of `{"order": "", "url": "", "image": "", "type": "}`
   
       `Items.order=[number]`   
 
       `Items.type=[number]`  Type of item, can be: 0 - deal; 1 - external 
             
       `Items.url=[string]`   The url when click to item. type is 0 then url is deal_id, type is 1 then url is a link
       
       `Items.image=[string]` The image url
       
     

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "name": "string",
        "position": "string",
        "items": [
          {
            "order": "string",
            "image": "string",
            "url": "string",
            "type": "number"
          },
          {...}
        ]
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
* **Sample Call:**


* **Notes:**


**List banners**
----
  List all banners

* **URL**

  /api/v1/banner

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**
 
   `position=[string]`

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": [
        {
          "id": "string",
          "name": "string",
          "position": "string",
          "items": [
            {
              "order": "string",
              "image": "string",
              "url": "string",
              "type": "number"
            },
            {...}
          ]
        }
      ],
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**


* **Notes:**


**Delete a banner**
----
  Delete a banner

* **URL**

  /api/v1/admin/banner/{banner_id}
  
* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "deleted": "boolean" 
      },
      "error":{ "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```

* **Notes:**

  
**Update a banner**
----
  Update a banner

* **URL**

  /api/v1/admin/banner/{banner_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Optional:**
   
   **Required:**
      
  `name=[string]`
  
  `position=[string]`
  
  `description=[string]`
  
  `order=[number]`
  
  `status=[number]`
  
  `items=[Array.<Items>]`    Array of object of `{"order": "", "url": "", "image": "", "type": "}`
  
      `Items.order=[number]`   

      `Items.type=[number]`  Type of item, can be: 0 - deal; 1 - external 
            
      `Items.url=[string]`   The url when click to item. type is 0 then url is deal_id, type is 1 then url is a link
      
      `Items.image=[string]` The image url

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "name": "string",
        "position": "string",
        "items": [
          {
            "order": "string",
            "image": "string",
            "url": "string",
            "type": "number"
          },
          {...}
        ]
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
* **Notes:**
  
