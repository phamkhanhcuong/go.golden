# type

## Branch

```
{
  "id": "string",
  "name": "string",
  "address": "string",
  "lat": "float",
  "lng": "float"
}
```

## Plan
```json
{
  "id": "number",
  "name": "string",
  "description": "string",
  "price": "number",
  "duration": "number",
  "order": "number",
  "status": "number",
  "create_at": "datetime",
  "update_at": "datetime"
}
```

## Card
```json
{
  "id": "number",
  "plan": "Plan",
  "name": "string",
  "activation_code": "string",
  "payment_method": "string",
  "expired_at": "datetime",
  "create_at": "datetime",
  "update_at": "datetime",
  "status": "number"
}
```