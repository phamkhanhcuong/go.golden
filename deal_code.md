**** == Deal Code For Admin == ****

--------
**Deal Code Import **
----

  Import deal code

* **URL**

  /api/v1/admin/deal_codes/import/:id

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    `file=[file]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
     {
       "error": {
         "code": 201,
         "message": "Đã tạo"
       },
       "data": {
         "total_imported" :"int"
       }
     }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```


* **Sample Call:**
  ```bash
  curl -X POST \
    http://localhost:8080/v1/admin/deal_codes/import/1 \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
  ```
  Response:
  ```json
  {
    "error": {
      "code": 201,
      "message": "Đã tạo"
    },
    "data": {
      "total_imported": "10"
    }
  }
  ```

* **Notes:**



**DealCode detail**
----

* **URL**

   /api/v1/admin/deal_codes/:id

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data":{
        "id": "string",
        "code": "string",
        "description": "string",
        "note": "string",
        "status": "int",
        "received_at": "datetime",
        "expired_at": "datetime",
        "created_at": "datetime",
        "updated_at": "datetime",
        "deal": "deal",
        "user": "user",
        "trasaction": "trasaction"
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```


* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/admin/deal_codes/3 \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": {
        "id": 3,
        "code": "test10003",
        "description": "",
        "note": "",
        "status": 1,
        "expired_at": "0001-01-01T00:00:00Z",
        "created_at": "2019-07-26T16:18:25.024936Z",
        "updated_at": "2019-07-26T16:18:25.024937Z",
        "received_at": "0001-01-01T00:00:00Z",
        "deal": {
          "id": 284,
          "deal_id": "",
          "type": "service",
          "name": "Phục Vụ Xuyên Đêm - Combo Hải Sản Hấp Dẫn tại Nhà hàng Saigon Marvel Hostel",
          "image_url": "https://storage.googleapis.com/golden-age/ce6ed9fc-b225-4e8f-9356-8d3a9c4281f6.jpg",
          "image_mobile": "",
          "quantity": 40,
          "num_of_voucher": 5,
          "description": "Khu phố Tây Bùi Viện được xem là nơi nhộn nhịp nhất ở Sài Gòn với rất nhiều hoạt động vui chơi, giải trí thâu đêm suốt sáng. Đây còn được xem là một “thiên đường ẩm thực” đa dạng với nhiều nhà hàng, quán ăn ngon. Trong đó, Saigon Marvel Hostel là một địa chỉ được đông đảo khách Việt sành ăn và du khách nước ngoài ghé đến nhờ thực đơn phong phú, món ăn ngon, giá cả phải chăng và không gian thoải mái.\n\n![](https://oms.hotdeal.vn/images/editors/sources/000356228089/356228-356228-body%20(37).jpg)\n\n\n- Phục vụ đa dạng các món ăn Việt – Á – Âu đặc sắc, hấp dẫn nhất là các loại hải sản tươi sống.\n\n Tất cả các món ăn đều được tẩm ướp và nêm nếm theo hương vị ẩm thực Việt, đậm đà bản sắc.\n\n- Nguồn nguyên liệu đầu vào tươi ngon, được tuyển chọn cẩn thận.\n\n- Nhà hàng có bố trí sẵn chỗ gửi xe và miễn phí gửi xe cho khách sử dụng.\n\n- Đội ngũ nhân viên trẻ trung, thân thiện, chu đáo sẽ góp phần mang đến sự hài lòng cho khách hàng.",
          "condition": "1/ Combo 1:\n\n- Tôm càng xào me\n\n- Nghêu hấp xả\n\n- Sò điệp nướng mỡ hành\n\n- Salad dầu giấm\n\n- Cháo cá\n\n- Combo bia/ nước ngọt\n\n2/ Combo 2:\n\n- Bạch tuộc hấp\n\n- Sò sữa nướng mỡ hành/ phô mai\n\n- Sò lụa hoa hấp sả\n\n- Salad dầu giấm\n\n- Lẩu cá\n\n- Combo bia/ nước ngọt\n\n3/ Combo 3:\n\n- Ốc bưu hấp tiêu\n\n- Sò dẹo nướng mỡ hành/phô mai\n\n- Sò huyết xào me\n\n- Cơm chiên hải sản\n\n- Salad dầu giấm\n\n- Combo bia/ nước ngọt\n\n4/ Combo 4:\n\n- Hàu nướng mỡ hành/ phô mai\n\n- Ốc móng tay xào rau muống\n\n- Nghêu hai còi xào chua ngọt\n\n- Cháo cá hải sản\n\n- Salad dầu giấm\n\n- Combo bia/ nước ngọt\n\n- Thời gian áp dụng: 10h - 10h hôm sau (24/24), tất cả các ngày trong tuần.\n\n- Sử dụng 01 voucher/ combo/ 2 người (không bù thêm tiền).\n\n- Không giới hạn số lượng voucher/ hóa đơn.",
          "note": "- Nhà hàng sẽ nhận tối thiểu từ 02 khách trở lên khi khách hàng có nhu cầu sử dụng voucher.\n\n- Khách hàng vui lòng liên hệ đặt bàn trước khi đến để được phục vụ tốt nhất.\n\n- Không áp dụng đồng thời với các chương trình khuyến mãi khác.\n\n- Phiếu không có giá trị quy đổi thành tiền mặt, không trả lại tiền thừa.\n\n- Thanh toán online nhận ngay voucher điện tử.\n\n- Thanh toán trả sau (COD) nhận voucher điện tử: MIỄN PHÍ.\n\n- Thanh toán trả sau nhận voucher giấy miễn phí cho đơn hàng từ 150.000vnđ trở lên, đơn hàng dưới 150.000vnđ thu phí giao hàng 9.000vnđ.",
          "rate": 5,
          "enable_booking": true,
          "enable_voucher": true,
          "private": false,
          "pre_charge": true,
          "min_booking_time": 120,
          "apply_day": "",
          "apply_hour": null,
          "price": 20000000,
          "regular_price": 0,
          "regular_discount": 100,
          "regular_discount_type": "p",
          "sale_price": 0,
          "sale_discount": 100,
          "sale_discount_type": "p",
          "order": 0,
          "status": 1,
          "hot": true,
          "featured": true,
          "started_at": "2019-07-20T17:00:00Z",
          "expired_at": "2021-07-26T17:00:00Z",
          "created_at": "2019-07-21T08:15:17.577799Z",
          "updated_at": "2019-07-24T11:03:43.482109Z",
          "extra": "",
          "category": {
            "id": 24,
            "name": "Ăn uống",
            "image_url": "",
            "mobile_url": "",
            "color": "",
            "icon": "https://storage.googleapis.com/golden-age/f319d396-a15c-4f5b-aa19-9d4f1e1cfcdf.png",
            "slug": "an-uong",
            "order": 2,
            "status": 1,
            "created_at": "2019-04-16T15:19:21.233264Z",
            "updated_at": "2019-07-21T18:57:51.573427Z"
          },
          "branches": null,
          "images": null,
          "merchant": null
        },
        "user": null,
        "trasaction": null
      }
    }
    ```

* **Notes:**




**DealCode list**
----

* **URL**

   /api/v1/admin/deal_codes

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**


* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
            "id": "string",
            "code": "string",
            "description": "string",
            "note": "string",
            "status": "int",
            "received_at": "datetime",
            "expired_at": "datetime",
            "created_at": "datetime",
            "updated_at": "datetime",
            "deal": "deal",
            "user": "user",
            "trasaction": "trasaction"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/admin/deal_codes \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": [
        {
          "id": 1,
          "code": "test10001",
          "description": "",
          "note": "",
          "status": 2,
          "expired_at": "0001-01-01T00:00:00Z",
          "created_at": "2019-07-26T16:18:24.967856Z",
          "updated_at": "2019-07-27T03:12:36.366511Z",
          "received_at": "2019-07-27T03:12:36.366503Z",
          "deal": {
            "id": 262,
            "deal_id": "",
            "type": "service",
            "name": "Silk Village Resort &Spa",
            "image_url": "https://storage.googleapis.com/golden-age/cd51bcfe-2da2-4699-8d50-e4b947368a63.jpg",
            "image_mobile": "",
            "quantity": 100000,
            "num_of_voucher": 0,
            "description": "- Hoi An Silk Village Resort & Spa tọa lạc tại thành phố Hội An thuộc tỉnh Quảng Nam, cách Phố Cổ Hội An 1,5 km. Được thiết kế giống như một làng lụa nguyên bản, chỗ nghỉ này có hồ bơi ngoài trời, khu vườn với bàn ghế cũng như trung tâm spa và chăm sóc sức khỏe. \n\n- Với chủ đề làng quê mộc mạc, du khách phải tản bộ qua những lối mòn trải đá trong vườn để đi từ đầu này đến đầu kia, mang lại cho du khách nhiều sự riêng tư.",
            "condition": "- Chương trình ưu đãi: Ưu đãi đặc quyền cho Chủ Thẻ Thành Viên Golden Age.\n\n- Chủ Thẻ sử dụng phải đặt phòng trước 3 tiếng thông qua Golden Age. (Trường hợp không đặt phòng trước, Golden Age không đảm bảo về ưu đãi).\n\n- Thời gian tiếp nhận đặt phòng từ 08:00 am sáng đến 18:00 (Từ thứ 2 đến thứ 7)\n\n**Lưu ý:**\n\n- Giá phòng đã bao gồm thuế phí và bữa sáng.\n\n- Không giải quyết những trường hợp đặt phòng không đúng thời gian theo quy định, hoặc đến khách sạn sau đó tiến hành đặt phòng.",
            "note": "",
            "rate": 5,
            "enable_booking": true,
            "enable_voucher": false,
            "private": true,
            "pre_charge": true,
            "min_booking_time": 180,
            "apply_day": "",
            "apply_hour": null,
            "price": 0,
            "regular_price": 0,
            "regular_discount": 0,
            "regular_discount_type": "p",
            "sale_price": 200000,
            "sale_discount": 0,
            "sale_discount_type": "p",
            "order": 0,
            "status": 1,
            "hot": false,
            "featured": true,
            "started_at": "2019-06-22T17:00:00Z",
            "expired_at": "2020-06-22T17:00:00Z",
            "created_at": "2019-06-23T09:19:23.478771Z",
            "updated_at": "2019-07-05T15:39:25.798657Z",
            "extra": "",
            "category": {
              "id": 23,
              "name": "Khách sạn",
              "image_url": "",
              "mobile_url": "",
              "color": "",
              "icon": "https://storage.googleapis.com/golden-age/51e7c849-d20c-4947-bc16-b829abbd1254.png",
              "slug": "khach-san",
              "order": 0,
              "status": 1,
              "created_at": "2019-04-16T15:10:08.895782Z",
              "updated_at": "2019-07-21T18:59:01.209743Z"
            },
            "branches": [
              {
                "id": 601,
                "branch_id": "",
                "merchant": {
                  "id": 243,
                  "name": "",
                  "image_url": "",
                  "image_mobile": "",
                  "description": "",
                  "slack_webhook_url": "",
                  "status": 0,
                  "created_at": "0001-01-01T00:00:00Z",
                  "updated_at": "0001-01-01T00:00:00Z",
                  "owner": null,
                  "category": null,
                  "city": null,
                  "photos": null
                },
                "deal": null,
                "name": "Silk Village Resort &Spa by Embrace",
                "slug": "silk-village-resort-spa-by-embrace",
                "image_url": "https://storage.googleapis.com/golden-age/cbded645-51ab-447c-93c7-aaf697a5b0af.jpg",
                "image_mobile": "",
                "description": "",
                "address": "28 Nguyễn Tất Thành, Phường Cẩm Phổ, Hội An, Quảng Nam, Việt Nam",
                "lat": 15.887305,
                "lng": 108.31904,
                "ratting": 0,
                "status": 1,
                "created_at": "2019-06-18T03:15:36.297039Z",
                "updated_at": "2019-06-24T10:29:05.997642Z"
              }
            ],
            "images": null,
            "merchant": {
              "id": 243,
              "name": "Silk Village Resort &Spa by Embrace",
              "image_url": "https://storage.googleapis.com/golden-age/ddb7d2ec-9211-4b31-a86a-ce14997edcd3.jpg",
              "image_mobile": "",
              "description": "",
              "slack_webhook_url": "",
              "status": 1,
              "created_at": "2019-06-18T03:12:17.885737Z",
              "updated_at": "2019-06-18T03:12:17.885738Z",
              "owner": {
                "id": 263,
                "user_id": "b0YxmoQOnElv9PGMvB5J",
                "balance": 0,
                "fingerprint": "",
                "phone_number": "",
                "_": "NJK6Y9QLP3",
                "card_number": "",
                "first_name": "Silk Village Resort &Spa by Embrace",
                "last_name": "",
                "dob": "",
                "gender": "",
                "avatar": "",
                "address": "",
                "cmnd": "",
                "email": "",
                "note": "",
                "role": "ROLE_MERCHANT",
                "created_at": "2019-06-18T03:12:17.873301Z",
                "updated_at": "2019-06-18T03:12:17.877267Z",
                "status": 1,
                "expired_at": "0001-01-01T00:00:00Z"
              },
              "category": {
                "id": 23,
                "name": "Khách sạn",
                "image_url": "",
                "mobile_url": "",
                "color": "",
                "icon": "https://storage.googleapis.com/golden-age/51e7c849-d20c-4947-bc16-b829abbd1254.png",
                "slug": "khach-san",
                "order": 0,
                "status": 1,
                "created_at": "2019-04-16T15:10:08.895782Z",
                "updated_at": "2019-07-21T18:59:01.209743Z"
              },
              "city": {
                "id": 68,
                "name": "Quảng Nam ",
                "order": 1,
                "status": 1,
                "created_at": "2019-05-28T08:27:05.647917Z",
                "updated_at": "2019-05-28T08:27:05.647919Z"
              },
              "photos": null
            }
          },
          "user": {
            "id": 272,
            "user_id": "4dxK0EyJn8wO9QrYP8pv",
            "balance": 9799.99,
            "fingerprint": "",
            "phone_number": "+84788448730",
            "_": "",
            "card_number": "",
            "first_name": "Pham Khanh Cuong",
            "last_name": "",
            "dob": "1985/01/12",
            "gender": "male",
            "avatar": "",
            "address": "HP",
            "cmnd": "",
            "email": "phamkhanhcuong@gmail.com",
            "note": "ghi chu",
            "role": "ROLE_USER",
            "created_at": "2019-07-04T10:18:14.56849Z",
            "updated_at": "2019-07-27T03:14:03.262059Z",
            "status": 1,
            "expired_at": "0001-01-01T00:00:00Z"
          },
          "trasaction": {
            "id": 6,
            "code": "0WPK8OG6L1QJ1347E95D",
            "type": "OrderDeal",
            "amount": 0.01,
            "amount_change": "-",
            "balance_before": 10000,
            "balance_after": 9999.99,
            "name": "Mua Deal",
            "description": "Silk Village Resort &Spa",
            "note": "",
            "user": {
              "id": 272,
              "user_id": "4dxK0EyJn8wO9QrYP8pv",
              "balance": 9799.99,
              "fingerprint": "",
              "phone_number": "+84788448730",
              "_": "",
              "card_number": "",
              "first_name": "Pham Khanh Cuong",
              "last_name": "",
              "dob": "1985/01/12",
              "gender": "male",
              "avatar": "",
              "address": "HP",
              "cmnd": "",
              "email": "phamkhanhcuong@gmail.com",
              "note": "ghi chu",
              "role": "ROLE_USER",
              "created_at": "2019-07-04T10:18:14.56849Z",
              "updated_at": "2019-07-27T03:14:03.262059Z",
              "status": 1,
              "expired_at": "0001-01-01T00:00:00Z"
            },
            "status": 1,
            "created_at": "2019-07-27T03:12:36.332844Z",
            "updated_at": "2019-07-27T03:12:36.344247Z",
            "data": null
          }
        },
        {
          "id": 3,
          "code": "test10003",
          "description": "",
          "note": "",
          "status": 1,
          "expired_at": "0001-01-01T00:00:00Z",
          "created_at": "2019-07-26T16:18:25.024936Z",
          "updated_at": "2019-07-26T16:18:25.024937Z",
          "received_at": "0001-01-01T00:00:00Z",
          "deal": {
            "id": 284,
            "deal_id": "",
            "type": "service",
            "name": "Phục Vụ Xuyên Đêm - Combo Hải Sản Hấp Dẫn tại Nhà hàng Saigon Marvel Hostel",
            "image_url": "https://storage.googleapis.com/golden-age/ce6ed9fc-b225-4e8f-9356-8d3a9c4281f6.jpg",
            "image_mobile": "",
            "quantity": 40,
            "num_of_voucher": 5,
            "description": "Khu phố Tây Bùi Viện được xem là nơi nhộn nhịp nhất ở Sài Gòn với rất nhiều hoạt động vui chơi, giải trí thâu đêm suốt sáng. Đây còn được xem là một “thiên đường ẩm thực” đa dạng với nhiều nhà hàng, quán ăn ngon. Trong đó, Saigon Marvel Hostel là một địa chỉ được đông đảo khách Việt sành ăn và du khách nước ngoài ghé đến nhờ thực đơn phong phú, món ăn ngon, giá cả phải chăng và không gian thoải mái.\n\n![](https://oms.hotdeal.vn/images/editors/sources/000356228089/356228-356228-body%20(37).jpg)\n\n\n- Phục vụ đa dạng các món ăn Việt – Á – Âu đặc sắc, hấp dẫn nhất là các loại hải sản tươi sống.\n\n Tất cả các món ăn đều được tẩm ướp và nêm nếm theo hương vị ẩm thực Việt, đậm đà bản sắc.\n\n- Nguồn nguyên liệu đầu vào tươi ngon, được tuyển chọn cẩn thận.\n\n- Nhà hàng có bố trí sẵn chỗ gửi xe và miễn phí gửi xe cho khách sử dụng.\n\n- Đội ngũ nhân viên trẻ trung, thân thiện, chu đáo sẽ góp phần mang đến sự hài lòng cho khách hàng.",
            "condition": "1/ Combo 1:\n\n- Tôm càng xào me\n\n- Nghêu hấp xả\n\n- Sò điệp nướng mỡ hành\n\n- Salad dầu giấm\n\n- Cháo cá\n\n- Combo bia/ nước ngọt\n\n2/ Combo 2:\n\n- Bạch tuộc hấp\n\n- Sò sữa nướng mỡ hành/ phô mai\n\n- Sò lụa hoa hấp sả\n\n- Salad dầu giấm\n\n- Lẩu cá\n\n- Combo bia/ nước ngọt\n\n3/ Combo 3:\n\n- Ốc bưu hấp tiêu\n\n- Sò dẹo nướng mỡ hành/phô mai\n\n- Sò huyết xào me\n\n- Cơm chiên hải sản\n\n- Salad dầu giấm\n\n- Combo bia/ nước ngọt\n\n4/ Combo 4:\n\n- Hàu nướng mỡ hành/ phô mai\n\n- Ốc móng tay xào rau muống\n\n- Nghêu hai còi xào chua ngọt\n\n- Cháo cá hải sản\n\n- Salad dầu giấm\n\n- Combo bia/ nước ngọt\n\n- Thời gian áp dụng: 10h - 10h hôm sau (24/24), tất cả các ngày trong tuần.\n\n- Sử dụng 01 voucher/ combo/ 2 người (không bù thêm tiền).\n\n- Không giới hạn số lượng voucher/ hóa đơn.",
            "note": "- Nhà hàng sẽ nhận tối thiểu từ 02 khách trở lên khi khách hàng có nhu cầu sử dụng voucher.\n\n- Khách hàng vui lòng liên hệ đặt bàn trước khi đến để được phục vụ tốt nhất.\n\n- Không áp dụng đồng thời với các chương trình khuyến mãi khác.\n\n- Phiếu không có giá trị quy đổi thành tiền mặt, không trả lại tiền thừa.\n\n- Thanh toán online nhận ngay voucher điện tử.\n\n- Thanh toán trả sau (COD) nhận voucher điện tử: MIỄN PHÍ.\n\n- Thanh toán trả sau nhận voucher giấy miễn phí cho đơn hàng từ 150.000vnđ trở lên, đơn hàng dưới 150.000vnđ thu phí giao hàng 9.000vnđ.",
            "rate": 5,
            "enable_booking": true,
            "enable_voucher": true,
            "private": false,
            "pre_charge": true,
            "min_booking_time": 120,
            "apply_day": "",
            "apply_hour": null,
            "price": 20000000,
            "regular_price": 0,
            "regular_discount": 100,
            "regular_discount_type": "p",
            "sale_price": 0,
            "sale_discount": 100,
            "sale_discount_type": "p",
            "order": 0,
            "status": 1,
            "hot": true,
            "featured": true,
            "started_at": "2019-07-20T17:00:00Z",
            "expired_at": "2021-07-26T17:00:00Z",
            "created_at": "2019-07-21T08:15:17.577799Z",
            "updated_at": "2019-07-24T11:03:43.482109Z",
            "extra": "",
            "category": {
              "id": 24,
              "name": "Ăn uống",
              "image_url": "",
              "mobile_url": "",
              "color": "",
              "icon": "https://storage.googleapis.com/golden-age/f319d396-a15c-4f5b-aa19-9d4f1e1cfcdf.png",
              "slug": "an-uong",
              "order": 2,
              "status": 1,
              "created_at": "2019-04-16T15:19:21.233264Z",
              "updated_at": "2019-07-21T18:57:51.573427Z"
            },
            "branches": [
              {
                "id": 672,
                "branch_id": "",
                "merchant": {
                  "id": 266,
                  "name": "",
                  "image_url": "",
                  "image_mobile": "",
                  "description": "",
                  "slack_webhook_url": "",
                  "status": 0,
                  "created_at": "0001-01-01T00:00:00Z",
                  "updated_at": "0001-01-01T00:00:00Z",
                  "owner": null,
                  "category": null,
                  "city": null,
                  "photos": null
                },
                "deal": null,
                "name": "Marvel Hostel Phạm Ngũ Lão",
                "slug": "marvel-hostel-pham-ngu-lao",
                "image_url": "",
                "image_mobile": "",
                "description": "",
                "address": "15 Phạm Ngũ Lão, Phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh, Việt Nam",
                "lat": 10.770476,
                "lng": 106.6988,
                "ratting": 0,
                "status": 1,
                "created_at": "2019-07-21T08:08:37.773787Z",
                "updated_at": "2019-07-21T08:08:37.773788Z"
              }
            ],
            "images": null,
            "merchant": {
              "id": 266,
              "name": "Nhà hàng Saigon Marvel Hostel Phạm Ngũ Lão Quận 1 Tp Hồ Chí Minh Việt Nam",
              "image_url": "https://storage.googleapis.com/golden-age/528f00e9-c502-4295-b87c-cbc3aeff6678.jpg",
              "image_mobile": "",
              "description": "Saigon Marvel Hostel là một địa chỉ lưu trú thuận tiện cho  khách du lịch nước ngoài và cũng là một địa điểm ăn uống lý tưởng cho cả thực khách bản địa lẫn khách du lịch muốn tìm hiểu ẩm thực Việt Nam.\n\nNhà hàng nằm ở tầng trệt của Marvel Hostel, có không gian hiện đại, ấm cúng, sạch sẽ, cộng với vị trí đắc địa ngay phố Tây khiến cho không khí thêm phần nhộn nhịp, sinh động.",
              "slack_webhook_url": "",
              "status": 1,
              "created_at": "2019-07-21T08:07:00.736966Z",
              "updated_at": "2019-07-22T08:12:06.387775Z",
              "owner": {
                "id": 348,
                "user_id": "6w80LqAlRZ80nymBp1Pv",
                "balance": 0,
                "fingerprint": "",
                "phone_number": "",
                "_": "4JM2PODWQV",
                "card_number": "",
                "first_name": "Nhà hàng Saigon Marvel Hostel Phạm Ngũ Lão",
                "last_name": "",
                "dob": "",
                "gender": "",
                "avatar": "",
                "address": "",
                "cmnd": "",
                "email": "",
                "note": "",
                "role": "ROLE_MERCHANT",
                "created_at": "2019-07-21T08:07:00.723773Z",
                "updated_at": "2019-07-21T08:07:00.728066Z",
                "status": 1,
                "expired_at": "0001-01-01T00:00:00Z"
              },
              "category": {
                "id": 24,
                "name": "Ăn uống",
                "image_url": "",
                "mobile_url": "",
                "color": "",
                "icon": "https://storage.googleapis.com/golden-age/f319d396-a15c-4f5b-aa19-9d4f1e1cfcdf.png",
                "slug": "an-uong",
                "order": 2,
                "status": 1,
                "created_at": "2019-04-16T15:19:21.233264Z",
                "updated_at": "2019-07-21T18:57:51.573427Z"
              },
              "city": {
                "id": 13,
                "name": "Hồ Chí Minh",
                "order": 1,
                "status": 1,
                "created_at": "2019-05-28T08:07:35.08347Z",
                "updated_at": "2019-05-28T08:08:50.496425Z"
              },
              "photos": null
            }
          },
          "user": null,
          "trasaction": null
        }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      }
    }
    ```

* **Notes:**

**** == DealCode For User == ****

--------


**DealCode detail**
----

* **URL**

   /api/v1/deal_codes/code/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data":{
        "id": "string",
        "code": "string",
        "description": "string",
        "note": "string",
        "status": "int",
        "received_at": "datetime",
        "expired_at": "datetime",
        "created_at": "datetime",
        "updated_at": "datetime",
        "deal": "deal",
        "user": "user",
        "trasaction": "trasaction"
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```


* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/deal_codes/code/test10003 \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

   ```json
   {
     "error": {
       "code": 200,
       "message": "Thành công"
     },
     "data": {
       "id": 1,
       "code": "test10001",
       "description": "",
       "note": "",
       "status": 2,
       "expired_at": "0001-01-01T00:00:00Z",
       "created_at": "2019-07-26T16:18:24.967856Z",
       "updated_at": "2019-07-27T03:12:36.366511Z",
       "received_at": "2019-07-27T03:12:36.366503Z",
       "deal": {
         "id": 262,
         "deal_id": "",
         "type": "service",
         "name": "Silk Village Resort &Spa",
         "image_url": "https://storage.googleapis.com/golden-age/cd51bcfe-2da2-4699-8d50-e4b947368a63.jpg",
         "image_mobile": "",
         "quantity": 100000,
         "num_of_voucher": 0,
         "description": "- Hoi An Silk Village Resort & Spa tọa lạc tại thành phố Hội An thuộc tỉnh Quảng Nam, cách Phố Cổ Hội An 1,5 km. Được thiết kế giống như một làng lụa nguyên bản, chỗ nghỉ này có hồ bơi ngoài trời, khu vườn với bàn ghế cũng như trung tâm spa và chăm sóc sức khỏe. \n\n- Với chủ đề làng quê mộc mạc, du khách phải tản bộ qua những lối mòn trải đá trong vườn để đi từ đầu này đến đầu kia, mang lại cho du khách nhiều sự riêng tư.",
         "condition": "- Chương trình ưu đãi: Ưu đãi đặc quyền cho Chủ Thẻ Thành Viên Golden Age.\n\n- Chủ Thẻ sử dụng phải đặt phòng trước 3 tiếng thông qua Golden Age. (Trường hợp không đặt phòng trước, Golden Age không đảm bảo về ưu đãi).\n\n- Thời gian tiếp nhận đặt phòng từ 08:00 am sáng đến 18:00 (Từ thứ 2 đến thứ 7)\n\n**Lưu ý:**\n\n- Giá phòng đã bao gồm thuế phí và bữa sáng.\n\n- Không giải quyết những trường hợp đặt phòng không đúng thời gian theo quy định, hoặc đến khách sạn sau đó tiến hành đặt phòng.",
         "note": "",
         "rate": 5,
         "enable_booking": true,
         "enable_voucher": false,
         "private": true,
         "pre_charge": true,
         "min_booking_time": 180,
         "apply_day": "",
         "apply_hour": null,
         "price": 0,
         "regular_price": 0,
         "regular_discount": 0,
         "regular_discount_type": "p",
         "sale_price": 200000,
         "sale_discount": 0,
         "sale_discount_type": "p",
         "order": 0,
         "status": 1,
         "hot": false,
         "featured": true,
         "started_at": "2019-06-22T17:00:00Z",
         "expired_at": "2020-06-22T17:00:00Z",
         "created_at": "2019-06-23T09:19:23.478771Z",
         "updated_at": "2019-07-05T15:39:25.798657Z",
         "extra": "",
         "category": {
           "id": 23,
           "name": "Khách sạn",
           "image_url": "",
           "mobile_url": "",
           "color": "",
           "icon": "https://storage.googleapis.com/golden-age/51e7c849-d20c-4947-bc16-b829abbd1254.png",
           "slug": "khach-san",
           "order": 0,
           "status": 1,
           "created_at": "2019-04-16T15:10:08.895782Z",
           "updated_at": "2019-07-21T18:59:01.209743Z"
         },
         "branches": null,
         "images": null,
         "merchant": null
       },
       "user": {
         "id": 272,
         "user_id": "4dxK0EyJn8wO9QrYP8pv",
         "balance": 9799.99,
         "fingerprint": "",
         "phone_number": "+84788448730",
         "_": "",
         "card_number": "",
         "first_name": "Pham Khanh Cuong",
         "last_name": "",
         "dob": "1985/01/12",
         "gender": "male",
         "avatar": "",
         "address": "HP",
         "cmnd": "",
         "email": "phamkhanhcuong@gmail.com",
         "note": "ghi chu",
         "role": "ROLE_USER",
         "created_at": "2019-07-04T10:18:14.56849Z",
         "updated_at": "2019-07-27T03:14:03.262059Z",
         "status": 1,
         "expired_at": "0001-01-01T00:00:00Z"
       },
       "trasaction": {
         "id": 6,
         "code": "0WPK8OG6L1QJ1347E95D",
         "type": "OrderDeal",
         "amount": 0.01,
         "amount_change": "-",
         "balance_before": 10000,
         "balance_after": 9999.99,
         "name": "Mua Deal",
         "description": "Silk Village Resort &Spa",
         "note": "",
         "user": {
           "id": 272,
           "user_id": "4dxK0EyJn8wO9QrYP8pv",
           "balance": 9799.99,
           "fingerprint": "",
           "phone_number": "+84788448730",
           "_": "",
           "card_number": "",
           "first_name": "Pham Khanh Cuong",
           "last_name": "",
           "dob": "1985/01/12",
           "gender": "male",
           "avatar": "",
           "address": "HP",
           "cmnd": "",
           "email": "phamkhanhcuong@gmail.com",
           "note": "ghi chu",
           "role": "ROLE_USER",
           "created_at": "2019-07-04T10:18:14.56849Z",
           "updated_at": "2019-07-27T03:14:03.262059Z",
           "status": 1,
           "expired_at": "0001-01-01T00:00:00Z"
         },
         "status": 1,
         "created_at": "2019-07-27T03:12:36.332844Z",
         "updated_at": "2019-07-27T03:12:36.344247Z",
         "data": null
       }
     }
   }
   ```

* **Notes:**

    


**DealCode list**
----

* **URL**

   /api/v1/deal_codes

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**


* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
            "id": "string",
            "code": "string",
            "description": "string",
            "note": "string",
            "status": "int",
            "received_at": "datetime",
            "expired_at": "datetime",
            "created_at": "datetime",
            "updated_at": "datetime",
            "deal": "deal",
            "user": "user",
            "trasaction": "trasaction"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

    ```bash
    curl -X POST \
      http://localhost:8080/v1/deal_codes \
      -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \

    ```
    Response:

    ```json
    {
      "error": {
        "code": 200,
        "message": "Thành công"
      },
      "data": [
        {
           "id": 1,
           "code": "test10001",
           "description": "",
           "note": "",
           "status": 2,
           "expired_at": "0001-01-01T00:00:00Z",
           "created_at": "2019-07-26T16:18:24.967856Z",
           "updated_at": "2019-07-27T03:12:36.366511Z",
           "received_at": "2019-07-27T03:12:36.366503Z",
           "deal": {
             "id": 262,
             "deal_id": "",
             "type": "service",
             "name": "Silk Village Resort &Spa",
             "image_url": "https://storage.googleapis.com/golden-age/cd51bcfe-2da2-4699-8d50-e4b947368a63.jpg",
             "image_mobile": "",
             "quantity": 100000,
             "num_of_voucher": 0,
             "description": "- Hoi An Silk Village Resort & Spa tọa lạc tại thành phố Hội An thuộc tỉnh Quảng Nam, cách Phố Cổ Hội An 1,5 km. Được thiết kế giống như một làng lụa nguyên bản, chỗ nghỉ này có hồ bơi ngoài trời, khu vườn với bàn ghế cũng như trung tâm spa và chăm sóc sức khỏe. \n\n- Với chủ đề làng quê mộc mạc, du khách phải tản bộ qua những lối mòn trải đá trong vườn để đi từ đầu này đến đầu kia, mang lại cho du khách nhiều sự riêng tư.",
             "condition": "- Chương trình ưu đãi: Ưu đãi đặc quyền cho Chủ Thẻ Thành Viên Golden Age.\n\n- Chủ Thẻ sử dụng phải đặt phòng trước 3 tiếng thông qua Golden Age. (Trường hợp không đặt phòng trước, Golden Age không đảm bảo về ưu đãi).\n\n- Thời gian tiếp nhận đặt phòng từ 08:00 am sáng đến 18:00 (Từ thứ 2 đến thứ 7)\n\n**Lưu ý:**\n\n- Giá phòng đã bao gồm thuế phí và bữa sáng.\n\n- Không giải quyết những trường hợp đặt phòng không đúng thời gian theo quy định, hoặc đến khách sạn sau đó tiến hành đặt phòng.",
             "note": "",
             "rate": 5,
             "enable_booking": true,
             "enable_voucher": false,
             "private": true,
             "pre_charge": true,
             "min_booking_time": 180,
             "apply_day": "",
             "apply_hour": null,
             "price": 0,
             "regular_price": 0,
             "regular_discount": 0,
             "regular_discount_type": "p",
             "sale_price": 200000,
             "sale_discount": 0,
             "sale_discount_type": "p",
             "order": 0,
             "status": 1,
             "hot": false,
             "featured": true,
             "started_at": "2019-06-22T17:00:00Z",
             "expired_at": "2020-06-22T17:00:00Z",
             "created_at": "2019-06-23T09:19:23.478771Z",
             "updated_at": "2019-07-05T15:39:25.798657Z",
             "extra": "",
             "category": {
               "id": 23,
               "name": "Khách sạn",
               "image_url": "",
               "mobile_url": "",
               "color": "",
               "icon": "https://storage.googleapis.com/golden-age/51e7c849-d20c-4947-bc16-b829abbd1254.png",
               "slug": "khach-san",
               "order": 0,
               "status": 1,
               "created_at": "2019-04-16T15:10:08.895782Z",
               "updated_at": "2019-07-21T18:59:01.209743Z"
             },
             "branches": null,
             "images": null,
             "merchant": null
           },
           "user": {
             "id": 272,
             "user_id": "4dxK0EyJn8wO9QrYP8pv",
             "balance": 9799.99,
             "fingerprint": "",
             "phone_number": "+84788448730",
             "_": "",
             "card_number": "",
             "first_name": "Pham Khanh Cuong",
             "last_name": "",
             "dob": "1985/01/12",
             "gender": "male",
             "avatar": "",
             "address": "HP",
             "cmnd": "",
             "email": "phamkhanhcuong@gmail.com",
             "note": "ghi chu",
             "role": "ROLE_USER",
             "created_at": "2019-07-04T10:18:14.56849Z",
             "updated_at": "2019-07-27T03:14:03.262059Z",
             "status": 1,
             "expired_at": "0001-01-01T00:00:00Z"
           },
           "trasaction": {
             "id": 6,
             "code": "0WPK8OG6L1QJ1347E95D",
             "type": "OrderDeal",
             "amount": 0.01,
             "amount_change": "-",
             "balance_before": 10000,
             "balance_after": 9999.99,
             "name": "Mua Deal",
             "description": "Silk Village Resort &Spa",
             "note": "",
             "user": {
               "id": 272,
               "user_id": "4dxK0EyJn8wO9QrYP8pv",
               "balance": 9799.99,
               "fingerprint": "",
               "phone_number": "+84788448730",
               "_": "",
               "card_number": "",
               "first_name": "Pham Khanh Cuong",
               "last_name": "",
               "dob": "1985/01/12",
               "gender": "male",
               "avatar": "",
               "address": "HP",
               "cmnd": "",
               "email": "phamkhanhcuong@gmail.com",
               "note": "ghi chu",
               "role": "ROLE_USER",
               "created_at": "2019-07-04T10:18:14.56849Z",
               "updated_at": "2019-07-27T03:14:03.262059Z",
               "status": 1,
               "expired_at": "0001-01-01T00:00:00Z"
             },
             "status": 1,
             "created_at": "2019-07-27T03:12:36.332844Z",
             "updated_at": "2019-07-27T03:12:36.344247Z",
             "data": null
           }
         }
      ],
      "metadata": {
        "total": 1,
        "result": 1,
        "page": 1,
        "limit": 10
      }
    }
    ```

* **Notes:**


