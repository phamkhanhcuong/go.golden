**ADMIN**
-----------------
**Upload a file**
----
  Upload a file

* **URL**

  /api/v1/admin/files

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Form Params**

  **Required:**
   
   `file=[FILE]`

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "file_id": "string",
        "file_name": "string",
        "file_url": "string",
      }
      "error": {
        "code": 201,
        "message": "Created"
      },
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
* **Sample Call:**


* **Notes:**


**List files**
----
  List all files

* **URL**

  /api/v1/admin/files

* **Headers**

  `Authorization: Bearer <token>`


* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**

    `query=[string]`   Filter. e.g. category:1,name:shop

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": [
        {
          "id": "string",
          "name": "string",
          "origin": "string",
          "size": "int",
          "type": "string",
          "file_url": "string",
          "status": "string",
          "created_at": "string",
          "updated_at": "string",
        },
        {...}

      ],
      "metadata": {
        "total": "int",
        "result": "int",
        "page": "int",
        "limit": "int"
      },
      "error": { "code": 202 }
    }
    ```

* **Error Response:**


* **Notes:**


**Retrieve File**
----
  Retrieve a file

* **URL**

  /api/v1/files/{file_id}

* **Method:**

  `GET`

*  **URL Params**

   None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "name": "string",
        "origin": "string",
        "size": "int",
        "type": "string",
        "file_url": "string",
        "status": "string",
        "created_at": "string",
        "updated_at": "string",
      },
      "error": { "code": 200 }
    }
    ```


* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 404 Not found  <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```


**Delete a file**
----
  Delete a file

* **URL**

  /api/v1/admin/files/{file_id}
  
* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": {
        "id": "string",
        "deleted": "boolean" 
      },

      "error": { "code": 202, "message": "Accepted" }

    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```

* **Notes:**




**USER**
-----------------
**Upload a file**
----
  Upload a file

* **URL**

  /api/v1/files

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Form Params**

  **Required:**

   `file=[FILE]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "file_id": "string",
        "file_name": "string",
        "file_url": "string",
      }
      "error": {
        "code": 201,
        "message": "Created"
      },
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**


* **Notes:**


**Delete a file**
----
  Delete a file

* **URL**

  /api/v1/files/{file_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `DELETE`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "deleted": "boolean"
      },

      "error": { "code": 202, "message": "Accepted" }

    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```

* **Notes:**

