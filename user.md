**Create anonymous user**
----
  Create anonymous user.

* **URL**

  /api/v1/users

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params** 
  
   **Required:**
 
   `fingerprint=[string]`     fingerprint
  

* **Success Response:**
  
  * **Status Code:** 201 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "fingerprint": "string",
        "status": "number"
      },
      "error": {
        "code": 201
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```


* **Notes:**


**Retrieve user**
----
  Retrieves the details of a user.

* **URL**

  /api/v1/users/{user_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `GET`
  
*  **URL Params**

   None

* **Data Params** 
  
   None
  
* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "name": "string",
        "phone": "string",
        "email": "string",
        "address": "string",
        "id_number": "string",
        "avatar": "string",
        "card": "Card",
        "unread_notification_count": "int"
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

* **Sample Call:**

```bash
curl -X GET \
  http://localhost:8086/v1/users/JOWvqG3jaX69lP7Dzb8E \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDU4NDIsImlhdCI6MTU1MjY0NDk0MiwiUGhvbmVOdW1iZXIiOiIiLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfQURNSU4ifQ.zHOGERD4lsH3YOjacQbFEhGgkRs7SSYoXqxDtfyWqq4'
```

* **Notes:**


**Update a user**
----
  Update a user. Any parameters not provided will be left unchanged.

* **URL**

  /api/v1/user/{usr_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `PUT`
  
*  **URL Params**

   None

* **Data Params** 
  
   `name=[string]`  Full name of user
   
   `dob=[string]`   Date of birth (MM/DD/YYYY)
   
   `email=[string]`   Email
   
   `address=[string]`   Email
   
   `avatar=[string]`    Link of avatar
  

* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "name": "string",
        "phone_number": "string",
        "dob": "string",
        "email": "string",
        "avatar": "string",
        "status": "number"
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

* **Sample Call:**

```bash
curl -X PUT \
  http://localhost:8086/v1/users/JOWvqG3jaX69lP7Dzb8E \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NjQxNDMsImlhdCI6MTU1MjY2MzI0MywiUGhvbmVOdW1iZXIiOiIiLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfQURNSU4ifQ.ipXK_NCqp9yN4p4KbPf2-LEkZIoD9m_-oNHzLcmjiNo' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 0f37075b-b58a-3769-9d62-9a794be99ea9' \
  -d '{
	"first_name":"first name ",
	"last_name": "last name "
}'
```

* **Notes:**



**Admin Create a user**
----
  Admin Create a user.

* **URL**

  /api/v1/admin/users

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params** 
  
   **Required:**
 
   `name=[string]`
   
   `dob=[string]`   Date of birth (MM/DD/YYYY)

   `phone_number=[string]`

   `email=[string]`

   `address=[string]`  
   
   `role=[string]`
   
   `id_card=[string]`
   
   `plan_id=[integer]`
      
* **Success Response:**
  
  * **Status Code:** 201 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "name": "string",
        "phone_number": "string",
        "email": "string",
        "dob": "string",
        "address": "string",
        "activation_code": "string",
        "status": "number"
      },
      "error": {
        "code": 201
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```


* **Notes:**
- Add create the normal user then it auto generate activation code. This code only used one time
- 

**Admin update a user**
----
  Admin update a user. Any parameters not provided will be left unchanged.

* **URL**

  /api/v1/admin/user/{user_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `PUT`
  
*  **URL Params**

   None

* **Data Params** 
  
   `name=[string]`  Full name of user
   
   `dob=[string]`   Date of birth (MM/DD/YYYY)
   
   `email=[string]`   Email
   
   `address=[string]`   Email
   
   `status=[number]`    Link of avatar
  

* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "name": "string",
        "phone_number": "string",
        "dob": "string",
        "email": "string",
        "avatar": "string",
        "status": "number"
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

* **Notes:**


**List all users**
----
  Returns a list of users 

* **URL**

  /api/v1/admin/users

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**
 
    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `phone_number=[string]` 
    
    `name=[string]`
    
    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2
    
    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc
    
    `query=[string]`   Filter. e.g. phone_number:+84793456899,email:test@test.com
    
* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": [
        {
          "id": "string",
          "name": "string",
          "phone_number": "string",
          "dob": "string",
          "email": "string",
          "avatar": "string",
          "status": "number",
          "card": "Card"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

* **Notes:**


**Admin delete a user**
----
  Admin Delete a user

* **URL**

  /api/v1/admin/users/{user_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
  
  * **Status Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The user ID does not exist"
      } 
    }
    ```
  
* **Notes:**

  - condition to delete an user
  - when matched with delete condition then mark `deleted=true` not real delete

