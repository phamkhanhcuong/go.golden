**Static pages**

**Create a page**
----
  Create a page

* **URL**

  /api/v1/admin/pages

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

   **Required:**

  `page_id=[string]`

   **Optional:**

  `title=[string]`

  `slug=[string]`

  `content=[string]`

  `type=[string]`


* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "slug": "string",
          "title": "string",
          "type": "string",
          "created_at": "datetime",
          "update_at": "datetime",
          "status": "boolean",
        }
      ],
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
        "error" : {
            "code": 400,
            "message": "The request is invalid"
        }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```

* **Notes:**


**List pages**
----
  List all of pages

* **URL**

  /api/v1/pages

* **Method:**

  `GET`

*  **URL Params**

    **Optional:**

       `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

       `offset=[integer]` The offset of the items returned. the default is 0

       `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

       `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

       `query=[string]`   Filter. e.g. category:1,name:shop

* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "string",
          "slug": "string",
          "title": "string",
          "type": "string",
          "created_at": "datetime",
          "update_at": "datetime",
          "status": "boolean",
        }
      ],
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      }
    }
    ```


* **Notes:**


**Detail page**
----

  Get C of static page

* **URL**

  /api/v1/pages/{slug}

* **Method:**
  
  `GET`
  
*  **URL Params**
  
  None
  
* **Data Params** 
  
  None
  
* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "error": { "code": 200 },
      "data": {
        "id": "number",
        "title": "string",
        "slug": "string",
        "content": "string",
        "type": "string",
        "created_at": "datetime",
        "updated_at": "datetime",
        "status": "boolean"
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```


* **Notes:**





**Update a page**
----
  Update a page

* **URL**

  /api/v1/admin/pages

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

   **Required:**

  `page_id=[string]`
  
   **Optional:**

  `title=[string]`
  
  `slug=[string]`
  
  `content=[string]`
  
  `type=[string]`
  

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": [
        {
          "id": "string",
          "slug": "string",
          "title": "string",
          "type": "string",
          "created_at": "datetime",
          "update_at": "datetime",
          "status": "boolean",
        }
      ],
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
        "error" : {
            "code": 400,
            "message": "The request is invalid"
        } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
* **Notes:**


**Delete a page**
----
  Delete a page

* **URL**

  /api/v1/admin/pages/{page_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "error":{ "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```

  * **Status Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
    
    
* **Notes:**


