**Create a deal**
----
  Create a deal

* **URL**

  /api/v1/admin/deals

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
   `category_id=[integer]`
   
   `branch_id=[[integer]]`  The list of branches applied
   
   `name=[string]`
   
   `type=[string]`    Type can be `service`, `hotel` or `airline`
   
   `image_url=[string]`
   
   `regular_discount=[integer]`
   
   `regular_discount_type=[string]`   discount type can be `p` or `f`. p: percent, f: fixed
   
   `sale_discount=[integer]`
   
   `sale_discount_type=[string]`   discount type can be `p` or `f`
   
   `price=[integer]`
   
   `regular_price=[integer]`
   
   `seller_price=[integer]`
   
   `private=[bool]`   the private deal. only for premium user. default false
     
   `enable_booking=[bool]`  default false
   
   `enable_voucher=[bool]`  default false
   
   `pre_charge=[bool]`  charge in advance. default false
   
   
   **Optional:**
  
    `quantity=[integer]`   The number of voucher. The default is null, this mean unlimited
    
    `description=[string]`
    
    `hot=[boolean]`  The default is false
    
    `expired_at=[string]`  The expiration date of deal  (MM/DD/YYYY)
    
    `min_booking_time=[string]`  Min time advance reservation. default is 0

    `images=[Array.<Items>]`    Array of object of `{"order": "", "image_url": ""}`

       `Items.order=[number]`
       `Items.image_url=[string]` The image url

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "category_id": "string",
        "name": "string",
        "type": "string",
        "image_url": "string",
        "regular_discount": "integer",
        "regular_discount_type": "string",
        "seller_discount": "integer",
        "seller_discount_type": "string",
        "quantity": "string",
        "description": "string",
        "hot": "boolean",
        "price": "integer",
        "regular_price": "integer",
        "sale_price": "integer",
        "branches": [Branch],
        "started_at": "datetime",
        "expired_at": "datetime",
        "created_at": "datetime",
        "updated_at": "datetime"
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
    
* **Notes:**

  

**List deals**
----
  List all deals

* **URL**

  /api/v1/deals

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**
 
    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.
   
    `offset=[integer]` The offset of the items returned. the default is 0
    
    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2
        
    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc
    
    `query=[string]`   Filter. e.g. category:1,name:shop

    
* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": [
        {
          "id": "string",
          "title": "string",
          "image": "string",
          "description": "string",
          "discount": "integer",
          "merchant": {
            "id": "string",
            "name": "string",
            "image": "string",
            "rate": "integer"
          },
          "branches": [Branch],
          "images": [Image]
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**



* **Notes:**



**Retrieve a deal**
----
  Retrieves the details of a deal.

* **URL**

  /api/v1/deals/{promotion_id}

* **Method:**
  
  `GET`
  
*  **URL Params**

   None

* **Data Params** 
  
   None
  
* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "title": "string",
        "image": "string",
        "description": "string",
        "discount": "integer",
        "merchant": {
          "id": "string",
          "name": "string",
          "image": "string",
          "rate": "integer"
        },
        "branches": [Branch],
        "images": [Image]

      },
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```

* **Notes:**


**Delete a deal**
----
  Delete a deal

* **URL**

  /api/v1/admin/deals/{deal_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
  
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
  
  
* **Notes:**




