**Create a category**
----
  Create a category

* **URL**

  /api/v1/admin/categories

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    **Required:**

   `name=[string]`

   **Optional:**

    `slug=[string]`

    `image_url=[string]`

    `order=[int]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data": {
          "id": "string",
          "name": "string",
          "slug": "string",
          "image_url": "string",
          "order": "int"
          "status": "bool"
          "created_at": "datetime",
          "updated_at": "datetime"

        },
        "error": {
          "code": 200
        }
      }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**



**List categories**
----
  Returns a list of categories

* **URL**

  /api/v1/categories

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**
query=Name:b&fields=Name,Id,Order&sortby=Order&order=desc&limit=10&offset=0

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop


* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data": [
          {
            "id": "string",
            "name": "string",
            "slug": "string",
            "image_url": "string",
            "order": "int"
            "status": "bool",
            "created_at": "datetime",
            "updated_at": "datetime"

          },
          { ... }
        ],
        "metadata": {
          "total": 2,
          "result": 2,
          "page": 1,
          "limit": 10
        },
        "error": { "code": 200 }
      }
    ```

* **Error Response:**



* **Notes:**




**Detail categories**
----
  Returns a list of categories

* **URL**

  /api/v1/categories/[:id]

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data": [
          {
            "id": "string",
            "name": "string",
            "slug": "string",
            "image_url": "string",
            "order": "int"
            "status": "bool",
            "created_at": "datetime",
            "updated_at": "datetime"

          },
          { ... }
        ],
        "metadata": {
          "total": 2,
          "result": 2,
          "page": 1,
          "limit": 10
        },
        "error": { "code": 200 }
      }
    ```

* **Error Response:**



* **Notes:**



**Update a category**
----
  Update a category

* **URL**

  /api/v1/admin/categories/{category_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    **Required:**

    `name=[string]`

    **Optional:**

    `slug=[string]`

    `image_url=[string]`

    `order=[int]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "name": "string",
        "slug": "string",
        "image_url": "string",
        "order" : "int",
        "status": "bool"
        "created_at": "datetime",
        "updated_at": "datetime"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
 
* **Notes:**


**Delete a category**
----
  Delete a category

* **URL**

  /api/v1/admin/categories/{category_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
      
  * **Status Code:** 404 Not found  <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 404,
        "message": "The requested resource could not be found"
      } 
    }
    ```
      
* **Notes:**


