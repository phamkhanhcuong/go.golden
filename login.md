**Login**
----
  Login

* **URL**

  /api/v1/login

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params** 
  
   **Required:**
      
   `code=[string]`   Account kit code
   
   **Optional:**
    
   `name=[string]`    The full name of user
   
   `activation_code=[string]`    Activation code
  

* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "access_token": "string",
        "refresh_token": "string",
        "at_expired_at": "datetime",
        "rt_expired_at": "datetime"
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```

* **Sample Call:**

  ```bash
  curl -X POST \
    http://localhost:8086/v1/login \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -H 'postman-token: 4c055705-a0ab-aa83-689e-74eeaa053ed6' \
    -d '{
    "code":"test",
    "device_token": "test123123xxxxxxxxxxxx.xxxlan2"
  }'
  ```
  
  Response
  ```json
  {
      "error": {
          "code": 200,
          "message": "OK"
      },
      "data": {
          "user_id": "JOWvqG3jaX69lP7Dzb8E",
          "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDQxNDEsImlhdCI6MTU1MjY0MzI0MSwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.siLtj32gV9DuCWYQy-T3T_sfFcfHQ4Q95t32HOHsyFI",
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTMyNDgwNDEsImlhdCI6MTU1MjY0MzI0MSwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.ZOJa7N82f39wvVxx90nTsUmIYfAbyf3ZvKNjfCXOogw",
          "at_expired_at": 1552644141,
          "rt_expired_at": 1553248041
      }
  }
  ```

* **Notes:**

  [sequenceDiagram](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG5cblVzZXItPj4rQWNjb3VudCBLaXQ6IHBob25lIG51bWJlclxuXG5cbkFjY291bnQgS2l0LS0-Pi1Vc2VyOiBBdXRoIGNvZGVcblxuVXNlci0-PitCYWNrZW5kOiBjYWxsIGxvZ2luIGFwaSB3aXRoIGF1dGggY29kZSAmIGRldmljZSB0b2tlblxuXG5CYWNrZW5kLS0-Pi1Vc2VyOiBBY2Nlc3MgdG9rZW5cbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In19)


**Exchange Refresh token**
----
  Exchange refresh token for access token.

* **URL**

  /api/v1/login/refresh

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params** 
  
   **Required:**
      
   `refresh_token=[string]`   Refresh token
     
* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "access_token": "string",
        "refresh_token": "string",
        "at_expired_at": "datetime",
        "rt_expired_at": "datetime"
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```

* **Sample Call:**

  ```bash
  curl -X POST \
    http://localhost:8086/v1/login/refresh \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -H 'postman-token: cc5ae008-5e90-95cc-f9e6-a2486e18f2e0' \
    -d '{
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDQxNDEsImlhdCI6MTU1MjY0MzI0MSwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.siLtj32gV9DuCWYQy-T3T_sfFcfHQ4Q95t32HOHsyFI"
  }'
  ```
  
  Response
  ```json
  {
      "error": {
          "code": 200,
          "message": "OK"
      },
      "data": {
          "user_id": "JOWvqG3jaX69lP7Dzb8E",
          "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDQyMzgsImlhdCI6MTU1MjY0MzMzOCwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.M8jEgl-2W667PTs-D2W2Vxmo4uCQ1cZ9-o34-0Cq454",
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTMyNDgxMzgsImlhdCI6MTU1MjY0MzMzOCwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.qUgY9wkXyuRY7vN2z3MEgx2XXL0UKvuf553KsF3kxj4",
          "at_expired_at": 1552644238,
          "rt_expired_at": 1553248138
      }
  }
  ```

* **Notes:**



**Logout**
----
  Logout

* **URL**

  /api/v1/logout

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `GET`
  
*  **URL Params**

   None

* **Data Params** 
  
   None
     

* **Success Response:**
  
  * **Status Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {},
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```

* **Sample Call:**

  ```bash
  curl -X GET \
    http://localhost:8086/v1/logout \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDQyMzgsImlhdCI6MTU1MjY0MzMzOCwiaXNzIjoiKzg0NzkzMTI0NTY3IiwiUGhvbmVOdW1iZXIiOiIrODQ3OTMxMjQ1NjciLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.M8jEgl-2W667PTs-D2W2Vxmo4uCQ1cZ9-o34-0Cq454' \
    -H 'cache-control: no-cache'
  ```
  
  Response
  ```json
  {
      "error": {
          "code": 200,
          "message": "OK"
      },
      "data": null
  }
  ```

* **Notes:**
