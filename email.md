**Newsletter**
----
   Register to receive email's promotion information

* **URL**

  /api/v1/newsletter/register

* **Method:**
  
  `POST`
  
*  **URL Params**

* **Data Params**
    
    `email=[string]` User's email registered

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Code:** 400 Bad request  <br />
    **Content:**
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```

* **Notes:**



**Unsubscribe**
----
  Unsubscribe

* **URL**

  /api/v1/newsletter/unsubscribe

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
  `email=[string]`

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "error": { "code": 200 }
    }
    ```
 
* **Error Response:**

  
* **Notes:**

  
  