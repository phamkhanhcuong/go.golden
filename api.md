# APIs
---
### Note
- Access tokens used as a Bearer credential and transmitted in an HTTP Authorization header to the API.

  `Authorization: Bearer <token>`
  
---

[Banner](banner.md)

[Booking](booking.md)

[Card](card.md)

[Category](category.md)

[Deals](deal.md)

[Email](email.md)

[Login](login.md)

[Merchant](merchant.md) 

[Payment](payment.md)

[Plan](plan.md)

[Static Page](page.md)

[User](user.md)

[Voucher](voucher.md)



