**Booking service**
----

  Create Booking service

* **URL**

  /api/v1/booking/service

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`
  
*  **URL Params**

   None

* **Data Params**
    
    `note=[string]`
    
    `adults=[integer]`
    
    `children=[integer]`
    
    `infants=[integer]`
    
    `branch_id=[string]`  
    
    `checkin=[string]`  checkin time in ISO format (ISO 8601) 
    
* **Success Response:**
  
  * **Code:** 201 <br />
    **Content:** 
    ```json
    { 
      "data":{
          "id": "string",
          "code": "string"
      },
      "error": { "code": 201 } 
    }
    ```
 
* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
 
  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```


* **Sample Call:**
  ```bash
  curl -X POST \
    http://localhost:8080/v1/booking/service \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQyODc0NDEsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTUzNjgyNjQxLCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.dSdT9nqAozdfUozOleZYPm7_X6nTxUgpLSSQdPUTy2s' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    "deal_id":"YD8XN92VQVYQG76ELOKW",
    "branch_id":"ZWD7RPGK2P82EO10V386",
    "checkin":"2019-04-01T02:44:00.673Z",
    "adults":1,
    "children":0,
    "infant":0,
    "note":"note",
    "status":1
  }'
  ```
  Response:
  ```json
  {
      "error": {
          "code": 200,
          "message": "OK"
      },
      "data": {
          "id": 2,
          "code": "NVLQP869ZM5ZDRJK5GXY",
          "type": "service",
          "deal": {
              "id": 1,
              "deal_id": "9O7JRGK0562PZ4VD6Y2L",
              "type": "service",
              "name": "deal 1",
              "image_url": "https://via.placeholder.com/150",
              "quantity": 10,
              "num_of_voucher": 10,
              "description": "description",
              "min_booking_time": 0,
              "discount": 20,
              "discount_type": "p",
              "price": 100000,
              "regular_price": 95000,
              "sale_price": 76000,
              "order": 0,
              "status": 1,
              "hot": true,
              "featured": true,
              "started_at": "0001-01-01T00:00:00Z",
              "expired_at": "0001-01-01T00:00:00Z",
              "created_at": "2019-03-27T17:30:41.948406+07:00",
              "updated_at": "2019-03-27T17:30:41.948407+07:00"
          },
          "branch": {
              "id": 1,
              "branch_id": "JVO85E1ZYQJ4GWM60XRQ",
              "merchant": {
                  "id": 1,
                  "name": "Merchant1",
                  "image_url": "",
                  "description": "Merchant 1",
                  "status": 1,
                  "created_at": "2019-03-20T16:51:29.580274+07:00",
                  "updated_at": "2019-03-21T03:40:15.803068+07:00",
                  "owner": {
                      "id": 1,
                      "user_id": "",
                      "fingerprint": "",
                      "phone_number": "",
                      "first_name": "",
                      "last_name": "",
                      "dob": "",
                      "gender": "",
                      "avatar": "",
                      "email": "",
                      "note": "",
                      "roles": "",
                      "created_at": "0001-01-01T00:00:00Z",
                      "updated_at": "0001-01-01T00:00:00Z",
                      "status": 0
                  },
                  "category": {
                      "id": 1,
                      "name": "",
                      "image_url": "",
                      "color": "",
                      "icon": "",
                      "slug": "",
                      "order": 0,
                      "status": 0,
                      "created_at": "0001-01-01T00:00:00Z",
                      "updated_at": "0001-01-01T00:00:00Z"
                  },
                  "city": {
                      "id": 1,
                      "name": "",
                      "order": 0,
                      "status": 0,
                      "created_at": "0001-01-01T00:00:00Z",
                      "updated_at": "0001-01-01T00:00:00Z"
                  },
                  "photos": null
              },
              "deal": null,
              "name": "Branch 1",
              "slug": "branch-1",
              "image_url": "",
              "description": "",
              "lat": 0,
              "lng": 0,
              "ratting": 0,
              "status": 1,
              "created_at": "2019-03-20T16:52:23.54378+07:00",
              "updated_at": "2019-03-21T03:29:10.477369+07:00"
          },
          "user": {
              "id": 1,
              "user_id": "JOWvqG3jaX69lP7Dzb8E",
              "fingerprint": "",
              "phone_number": "0904888999",
              "first_name": "",
              "last_name": "",
              "dob": "",
              "gender": "",
              "avatar": "",
              "email": "",
              "note": "",
              "roles": "ROLE_ADMIN",
              "created_at": "2019-03-20T16:51:16.168998+07:00",
              "updated_at": "2019-03-26T12:26:51.620634+07:00",
              "status": 1
          },
          "price": 76000,
          "checkin": "2019-03-29T02:44:00.673Z",
          "checkout": "0001-01-01T00:00:00Z",
          "adults": 1,
          "children": 0,
          "infant": 0,
          "from": "",
          "to": "",
          "note": "note",
          "status": 1,
          "created_at": "2019-03-28T08:39:37.770686Z",
          "updated_at": "2019-03-28T08:39:37.772686Z"
      }
  }
  ```

* **Notes:**



**Booking airline**
----
  Booking airline

* **URL**

  /api/v1/booking/airline

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    `merchant_id=[string]`

    `name=[string]`

    `note=[string]`

    `adults=[integer]`

    `children=[integer]`

    `infants=[integer]`

    `checkin=[string]`   checkin time in ISO format (ISO 8601) 
    
    `checkout=[string]`   checkin time in ISO format (ISO 8601) 

    `from=[string]`

    `to=[string]`

    `round_trip=[bool]`

    `user[0..n][name]=[string]`

    `user[0..n][gender]=[string]`

    `user[0..n][phone_number]=[string]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
    {
      "data":{
        "id": "number",
        "code": "string",
        "type": "string",
        "deal": "Deal",
        "branch": "Branch",
        "merchant": "Merchant",
        "user": "User",
        "price": "number",
        "quantity": "number",
        "checkin": "string",
        "checkout": "string",
        "adults": "number",
        "children": "number",
        "infant": "number",
        "from": "string",
        "to": "string",
        "note": "string",
        "round_trip": "boolean",
        "status": "number",
        "created_at": "string",
        "updated_at": "string"
      },
      "error": { "code": 201 }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

* **Sample Call:**

  ```bash
  curl -X POST \
    http://localhost:8080/v1/booking/airline \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ3ODYyMDgsImp0aSI6IkJYTm1QZUpHUmUyOXpiQXkyMDZyIiwiaWF0IjoxNTU0MTgxNDA4LCJpc3MiOiIrODQ3OTMxMjQ1NjciLCJQaG9uZU51bWJlciI6Iis4NDc5MzEyNDU2NyIsIlVJRCI6MjUsIlVzZXJJRCI6IkJYTm1QZUpHUmUyOXpiQXkyMDZyIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.xOVHVv2eHCCCk4emeEyiZoBBi_hz1PV4WCbKqGWJqns' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
      "merchant_id": 6,
      "checkin":"2019-04-01T02:44:00.673Z",
      "checkout":"2019-06-01T02:44:00.673Z",
      "round_trip": true,
      "users": [
        {
          "name": "Dolores P Ector",
          "phone_number": "+16469616093",
          "gender": "female"
        }
      ],
        "adults": 1,
        "children": 0,
        "infant": 0,
        "from": "HPH",
        "to": "SGN",
        "note":"note",
        "status":1
      }'
  ```

* **Notes:**




**Booking hotel**
----
  Create Booking hotel

* **URL**

  /api/v1/booking/hotel

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

  None

* **Data Params**

  **Required:**

    `branch_id=[string]`

    `deal_id=[string]`  

    `checking=[string]`    checkin time in ISO format (ISO 8601)

    `checkout=[string]`      checkin time in ISO format (ISO 8601)

    `quantity=[integer]`    number of rooms


  **Optional:**

  `note=[string]`


* **Success Response:**

  * **Code:** 201 <br />
    **Content:**
    ```json
    {
      "data":{
          "id": "number",
          "code": "string",
          "type": "string",
          "deal": "Deal",
          "branch": "Branch",
          "merchant": "Merchant",
          "user": "User",
          "price": "number",
          "quantity": "number",
          "checkin": "string",
          "checkout": "string",
          "adults": "number",
          "children": "number",
          "infant": "number",
          "from": "string",
          "to": "string",
          "note": "string",
          "round_trip": "boolean",
          "status": "number",
          "created_at": "string",
          "updated_at": "string"
      },
      "error": { "code": 201 }
    }
    ```

* **Error Response:**

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
    
* **Sample Call:**

  ```bash
  curl -X POST \
    http://localhost:8080/v1/booking/service \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ3ODYyMDgsImp0aSI6IkJYTm1QZUpHUmUyOXpiQXkyMDZyIiwiaWF0IjoxNTU0MTgxNDA4LCJpc3MiOiIrODQ3OTMxMjQ1NjciLCJQaG9uZU51bWJlciI6Iis4NDc5MzEyNDU2NyIsIlVJRCI6MjUsIlVzZXJJRCI6IkJYTm1QZUpHUmUyOXpiQXkyMDZyIiwiUm9sZSI6IlJPTEVfVVNFUiJ9.xOVHVv2eHCCCk4emeEyiZoBBi_hz1PV4WCbKqGWJqns' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    "deal_id":"WRXOVYLG5M4PD82M69EZ",
    "branch_id":"O91Z3PG8YMX4KWRD5EN6",
    "checkin":"2019-04-01T02:44:00.673Z",
    "adults":1,
    "children":0,
    "infant":0,
    "note":"note",
    "status":1
  }'
  ```
  
* **Notes:**



**Booking detail**
----

* **URL**

   /api/v1/booking/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   None

* **Data Params**

   None

* **Success Response:**

  * **Status Code:** 200 <br />
    **Content:**
    ```json
    {
      "data":{
        "id": "number",
        "code": "string",
        "type": "string",
        "deal": "Deal",
        "branch": "Branch",
        "merchant": "Merchant",
        "user": "User",
        "price": "number",
        "quantity": "number",
        "checkin": "string",
        "checkout": "string",
        "adults": "number",
        "children": "number",
        "infant": "number",
        "from": "string",
        "to": "string",
        "note": "string",
        "round_trip": "boolean",
        "status": "number",
        "created_at": "string",
        "updated_at": "string"
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**

    


**Booking list**
----

* **URL**

   /api/v1/admin/booking

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

   `GET`

*  **URL Params**

   **Optional:**

   `user_id=[string]`
   
   `type=[string]`    The type of boooking. Can be `service` or `airline` or `hotel`.

   `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

   `offset=[integer]` The offset of the items returned. the default is 0


* **Data Params**

   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": [
        {
          "id": "number",
          "code": "string",
          "type": "string",
          "deal": "Deal",
          "branch": "Branch",
          "merchant": "Merchant",
          "user": "User",
          "price": "number",
          "quantity": "number",
          "checkin": "string",
          "checkout": "string",
          "adults": "number",
          "children": "number",
          "infant": "number",
          "from": "string",
          "to": "string",
          "note": "string",
          "round_trip": "boolean",
          "status": "number",
          "created_at": "string",
          "updated_at": "string"
        },
        { ... }
      ],
      "metadata": {
        "total": 2,
        "result": 2,
        "page": 1,
        "limit": 10
      },
      "error": { "code": 200 }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**


**Admin update service booking**
----
  Admin update service booking

* **URL**

  /v1/admin/booking/service/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `PATH`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
   `price=[number]`
   
   `status=[number]`    can be: 0 - reject, 4 - paid

* **Success Response:**
  
  * **Code:** 202 <br />
    **Content:** 
    ```json
    {
      "error": {
        "code": 202,
        "message": "Accepted"
      },
      "data": {
        "id": "number",
        "code": "string",
        "type": "string",
        "deal": "Deal",
        "branch": "Branch",
        "merchant": "Merchant",
        "user": "User",
        "price": "number",
        "quantity": "number",
        "checkin": "string",
        "checkout": "string",
        "adults": "number",
        "children": "number",
        "infant": "number",
        "from": "string",
        "to": "string",
        "note": "string",
        "round_trip": "boolean",
        "status": "number",
        "created_at": "string",
        "updated_at": "string"
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
      **Content:**
      ```json
      {
        "error" : {
          "code": 400,
          "message": "The request is invalid"
        }
      }
      ```
  
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

  ```bash
  curl -X PATCH \
    http://localhost:8080/v1/admin/booking/service/ER1MK0VPZ6G3QXG65DJ9 \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ3OTM1NTQsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTU0MTg4NzU0LCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.tfBRzQOszdzvO4llNMQeBXQGpDU0IMAoD7CRGKoM1-g' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    "price": 95000,
    "status": 3
  }'
  ```
  
  ```json
  {
    "error": {
      "code": 202,
      "message": "Accepted"
    },
    "data": {
      "id": 7,
      "code": "ER1MK0VPZ6G3QXG65DJ9",
      "type": "service",
      "deal": {
        "id": 5,
        "deal_id": "",
        "type": "service",
        "name": "deal 1 new",
        "image_url": "https://via.placeholder.com/150",
        "quantity": 10,
        "num_of_voucher": 10,
        "description": "description",
        "note": "",
        "rate": 0,
        "enable_booking": true,
        "enable_voucher": false,
        "private": false,
        "pre_charge": false,
        "min_booking_time": 0,
        "price": 100000,
        "regular_price": 95000,
        "regular_discount": 20,
        "regular_discount_type": "p",
        "sale_price": 76000,
        "sale_discount": 0,
        "sale_discount_type": "p",
        "order": 0,
        "status": 1,
        "hot": true,
        "featured": true,
        "started_at": "0001-01-01T00:00:00Z",
        "expired_at": "0001-01-01T00:00:00Z",
        "created_at": "2019-04-02T12:02:35.642366+07:00",
        "updated_at": "2019-04-02T12:02:35.642368+07:00",
        "category": {
          "id": 1,
          "name": "Category 1",
          "image_url": "",
          "color": "",
          "icon": "",
          "slug": "category-1",
          "order": 1,
          "status": 1,
          "created_at": "2019-03-20T16:46:37.098481+07:00",
          "updated_at": "2019-03-26T00:08:50.493917+07:00"
        },
        "branches": null
      },
      "branch": {
        "id": 2,
        "branch_id": "",
        "merchant": {
          "id": 2,
          "name": "Merchant2",
          "image_url": "",
          "description": "Merchant 1",
          "slack_webhook_url": "",
          "status": 1,
          "created_at": "2019-03-21T03:30:18.714665+07:00",
          "updated_at": "2019-03-21T03:51:25.849552+07:00",
          "owner": {
            "id": 1,
            "user_id": "",
            "fingerprint": "",
            "phone_number": "",
            "first_name": "",
            "last_name": "",
            "dob": "",
            "gender": "",
            "avatar": "",
            "address": "",
            "email": "",
            "note": "",
            "role": "",
            "created_at": "0001-01-01T00:00:00Z",
            "updated_at": "0001-01-01T00:00:00Z",
            "status": 0
          },
          "category": {
            "id": 1,
            "name": "",
            "image_url": "",
            "color": "",
            "icon": "",
            "slug": "",
            "order": 0,
            "status": 0,
            "created_at": "0001-01-01T00:00:00Z",
            "updated_at": "0001-01-01T00:00:00Z"
          },
          "city": {
            "id": 1,
            "name": "",
            "order": 0,
            "status": 0,
            "created_at": "0001-01-01T00:00:00Z",
            "updated_at": "0001-01-01T00:00:00Z"
          },
          "photos": null
        },
        "deal": null,
        "name": "Branch 1.1",
        "slug": "branch-1-1",
        "image_url": "",
        "description": "",
        "lat": 0,
        "lng": 0,
        "ratting": 0,
        "status": 1,
        "created_at": "2019-03-21T03:30:51.228583+07:00",
        "updated_at": "2019-03-21T03:30:51.228584+07:00"
      },
      "merchant": {
        "id": 2,
        "name": "Merchant2",
        "image_url": "",
        "description": "Merchant 1",
        "slack_webhook_url": "",
        "status": 1,
        "created_at": "2019-03-21T03:30:18.714665+07:00",
        "updated_at": "2019-03-21T03:51:25.849552+07:00",
        "owner": {
          "id": 1,
          "user_id": "",
          "fingerprint": "",
          "phone_number": "0904888999",
          "first_name": "",
          "last_name": "",
          "dob": "",
          "gender": "",
          "avatar": "",
          "address": "",
          "email": "",
          "note": "",
          "role": "ROLE_ADMIN",
          "created_at": "2019-03-20T16:51:16.168998+07:00",
          "updated_at": "2019-03-26T12:26:51.620634+07:00",
          "status": 1
        },
        "category": {
          "id": 1,
          "name": "Category 1",
          "image_url": "",
          "color": "",
          "icon": "",
          "slug": "category-1",
          "order": 1,
          "status": 1,
          "created_at": "2019-03-20T16:46:37.098481+07:00",
          "updated_at": "2019-03-26T00:08:50.493917+07:00"
        },
        "city": {
          "id": 1,
          "name": "HCM",
          "order": 1,
          "status": 1,
          "created_at": "2019-03-20T16:46:14.052069+07:00",
          "updated_at": "2019-03-21T02:42:05.535265+07:00"
        },
        "photos": null
      },
      "user": {
        "id": 25,
        "user_id": "BXNmPeJGRe29zbAy206r",
        "fingerprint": "",
        "phone_number": "+84793124567",
        "first_name": "test 11",
        "last_name": "abc",
        "dob": "1999/01/01",
        "gender": "male",
        "avatar": "https://via.placeholder.com/150",
        "address": "",
        "email": "tst@test.com",
        "note": "",
        "role": "ROLE_USER",
        "created_at": "2019-03-28T15:32:23.856424+07:00",
        "updated_at": "2019-03-28T16:27:21.894822+07:00",
        "status": 1
      },
      "price": 95000,
      "quantity": 1,
      "checkin": "2019-04-01T09:44:00.673+07:00",
      "checkout": "0001-01-01T00:00:00Z",
      "adults": 1,
      "children": 0,
      "infant": 0,
      "from": "",
      "to": "",
      "note": "note",
      "round_trip": false,
      "status": 3,
      "created_at": "2019-04-02T14:04:14.628863+07:00",
      "updated_at": "2019-04-02T08:53:12.888419Z",
      "metadata": null
    }
  }
  ```

* **Notes:**



**admin update hotel booking**
----
  admin update hotel booking

* **URL**

  /v1/admin/booking/hotel/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `PATCH`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
   `price=[number]`
   
   `status=[number]`    can be: 0 - reject, 4 - paid

* **Success Response:**
  
  * **Code:** 202 <br />
    **Content:** 
    ```json
    {
      "error": {
        "code": 202,
        "message": "Accepted"
      },
      "data": {
        "id": "number",
        "code": "string",
        "type": "string",
        "deal": "Deal",
        "branch": "Branch",
        "merchant": "Merchant",
        "user": "User",
        "price": "number",
        "quantity": "number",
        "checkin": "string",
        "checkout": "string",
        "adults": "number",
        "children": "number",
        "infant": "number",
        "from": "string",
        "to": "string",
        "note": "string",
        "round_trip": "boolean",
        "status": "number",
        "created_at": "string",
        "updated_at": "string"
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
  
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Sample Call:**

  ```bash
  curl -X PATCH \
    http://localhost:8080/v1/admin/booking/hotel/4MLV9DOR39M7Y0W86JNQ \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ3OTU4NTgsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTU0MTkxMDU4LCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.ySTMpO-s_kSkfkM58dEzyrkJn4ABVCL8uZAzPkCvxIc' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
    "price": 6000000,
    "status": 3
  }'
  ```
  
* **Notes:**



**Admin update airline booking**
----
  Admin update airline booking

* **URL**

  /v1/admin/booking/airline/{code}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `PATCH`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
     
   `price=[number]`
   
   `status=[number]`    can be: 0 - reject, 4 - paid
   
   `flight_code_1=[string]`
   
   `booking_code_1=[string]`
   
   `departures_1=[string]`
   
   `boarding_time_1=[string]`   time in ISO format (ISO 8601) 
   
   `landing_time_1=[string]`    time in ISO format (ISO 8601) 
   
   `flight_number_2=[string]`
   
   `booking_code_2=[string]`
   
   `departures_2=[string]`
   
   `boarding_time_2=[string]`   time in ISO format (ISO 8601) 
   
   `landing_time_2=[string]`    time in ISO format (ISO 8601) 
   

* **Success Response:**
  
  * **Code:** 202 <br />
    **Content:** 
    ```json
    {
      "error": {
        "code": 202,
        "message": "Accepted"
      },
      "data": {
        "id": 10,
        "code": "54QKDN1M3D47E68W9YVL",
        "type": "airline",
        "deal": null,
        "branch": null,
        "merchant": {
          "id": 6,
          "name": "Vietnam airline",
          "image_url": "https://via.placeholder.com/150",
          "description": "",
          "slack_webhook_url": "",
          "status": 1,
          "created_at": "2019-04-02T14:47:49.858281+07:00",
          "updated_at": "2019-04-02T14:47:49.858282+07:00",
          "owner": {
            "id": 30,
            "user_id": "wqEX7GpvRxM9AY3m56re",
            "fingerprint": "",
            "phone_number": "",
            "first_name": "Vietnam airline",
            "last_name": "",
            "dob": "",
            "gender": "",
            "avatar": "",
            "address": "",
            "email": "",
            "note": "",
            "role": "ROLE_MERCHANT",
            "created_at": "2019-04-02T14:47:49.82108+07:00",
            "updated_at": "2019-04-02T14:47:49.837304+07:00",
            "status": 1
          },
          "category": {
            "id": 2,
            "name": "Category 2",
            "image_url": "",
            "color": "",
            "icon": "",
            "slug": "category-2",
            "order": 1,
            "status": 1,
            "created_at": "2019-03-20T16:50:24.85862+07:00",
            "updated_at": "2019-03-20T16:50:24.85862+07:00"
          },
          "city": {
            "id": 6,
            "name": "Hải Phòng",
            "order": 1,
            "status": 1,
            "created_at": "2019-04-01T21:58:54.811406+07:00",
            "updated_at": "2019-04-01T21:58:54.811408+07:00"
          },
          "photos": null
        },
        "user": {
          "id": 25,
          "user_id": "BXNmPeJGRe29zbAy206r",
          "fingerprint": "",
          "phone_number": "+84793124567",
          "first_name": "test 11",
          "last_name": "abc",
          "dob": "1999/01/01",
          "gender": "male",
          "avatar": "https://via.placeholder.com/150",
          "address": "",
          "email": "tst@test.com",
          "note": "",
          "role": "ROLE_USER",
          "created_at": "2019-03-28T15:32:23.856424+07:00",
          "updated_at": "2019-03-28T16:27:21.894822+07:00",
          "status": 1
        },
        "price": 6000000,
        "quantity": 0,
        "checkin": "2019-04-01T09:44:00.673+07:00",
        "checkout": "2019-06-01T09:44:00.673+07:00",
        "adults": 0,
        "children": 0,
        "infant": 0,
        "from": "",
        "to": "",
        "note": "note",
        "round_trip": true,
        "status": 3,
        "created_at": "2019-04-02T15:04:52.913835+07:00",
        "updated_at": "2019-04-02T08:27:30.971409Z",
        "metadata": {
          "boarding_time_1": "2019-04-08T10:00:00Z",
          "boarding_time_2": "2019-04-20T13:40:00Z",
          "booking_code_1": "REU5KZ",
          "booking_code_2": "REU5KZ",
          "departures_1": "Sân bay Quốc tế Cát Bi - Nhà ga hành khách",
          "departures_2": "Sân bay quốc tế Tân Sơn Nhất - Khu C và D, Nhà ga Nội địa",
          "flight_code_1": "VN-625",
          "flight_number_2": "VN-622",
          "landing_time_1": "2019-04-08T12:45:00Z",
          "landing_time_2": "2019-04-20T15:40:00Z"
        }
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```
  
  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```
    
* **Sample Call:**

  ```bash
  curl -X PATCH \
    http://localhost:8080/v1/admin/booking/airline/54QKDN1M3D47E68W9YVL \
    -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ3OTU4NTgsImp0aSI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiaWF0IjoxNTU0MTkxMDU4LCJpc3MiOiIwOTA0ODg4OTk5IiwiUGhvbmVOdW1iZXIiOiIwOTA0ODg4OTk5IiwiVUlEIjoxLCJVc2VySUQiOiJKT1d2cUczamFYNjlsUDdEemI4RSIsIlJvbGUiOiJST0xFX0FETUlOIn0.ySTMpO-s_kSkfkM58dEzyrkJn4ABVCL8uZAzPkCvxIc' \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
  	"price": 6000000,
  	"status": 3,
  	"flight_code_1":"VN-625",
  	"booking_code_1": "REU5KZ",
  	"departures_1": "Sân bay Quốc tế Cát Bi - Nhà ga hành khách",
  	"boarding_time_1": "2019-04-08T10:00:00.000Z",
  	"landing_time_1": "2019-04-08T12:45:00.000Z",
  	"flight_number_2": "VN-622",
  	"booking_code_2": "REU5KZ",
  	"departures_2": "Sân bay quốc tế Tân Sơn Nhất - Khu C và D, Nhà ga Nội địa",
  	"boarding_time_2": "2019-04-20T13:40:00.000Z",
  	"landing_time_2": "2019-04-20T15:40:00.000Z"
  }'
  ```

* **Notes:**



