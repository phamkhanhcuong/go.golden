
**List settings**
----
  Returns a list of settings

* **URL**

  /api/v1/admin/settings

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**
query=Name:b&fields=Name,Id,Order&sortby=Order&order=desc&limit=10&offset=0

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. setting:1,name:shop


* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data":
          {
           "id": "int",
           "key": "string",
           "value": "string",
           "created_at": "datetime",
           "updated_at": "datetime"
          },
          { ... }
        ],
        "metadata": {
          "total": 1,
          "result": 1,
          "page": 1,
          "limit": 10
        },
        "error": { "code": 200 }
      }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**


**Detail settings**
----
  Returns a list of settings

* **URL**

  /api/v1/admin/settings/[:id]

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data":
        {
            "id": "int",
            "key": "string",
            "value": "string",
            "created_at": "datetime",
            "updated_at": "datetime"
        }

        "error": { "code": 200 }
      }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**


**Update a setting**
----
  Update a setting

* **URL**

  /api/v1/admin/settings/{setting_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

    **Required:**

    `key=[string]`
    `value=[string]`

    **Optional:**


* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
    {
      "data": {
        "id": "string",
        "key": "string",
        "value": "string",
        "created_at": "datetime",
        "updated_at": "datetime"
      },
      "error": {
        "code": 200
      }
    }
    ```

* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      }
    }
    ```

  * **Status Code:** 401 Unauthorized <br />
    **Content:**
    ```json
    {
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      }
    }
    ```

* **Notes:**


**Banks settings**
----
  Returns a list banks

* **URL**

  /api/v1/settings/banks

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data":[
            {
                "account_id": "00310001xxxxx",
                "account_name": "PHAM KHANH CUONG",
                "branch_name": "HAI PHONG",
                "full_name": "Vietcombank",
                "id": 1,
                "image_url": "https://media.dalatcity.org//Images/VLG/superadminportal.vlg/Ti%e1%bb%87n%20%c3%adch/ATM/636529926612553293_1507081911266atm-vietcom.png",
                "message": "",
                "name": "VBC"
            }
        ]

        "error": { "code": 200 }
      }
    ```

* **Error Response:**


* **Notes:**



**Holidays settings**
----
  Returns a list holidays

* **URL**

  /api/v1/settings/holidays

* **Method:**

  `GET`

*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
      {
        "data":[
            [
                30,
                4
            ],
            [
                1,
                5
            ]
        ]

        "error": { "code": 200 }
      }
    ```

* **Error Response:**


* **Notes:**
