**Create a plan**
----
  Create a plan

* **URL**

  /api/v1/admin/plans

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**
   
     `name=[string]`
     
     `price=[integer]`
     
     `duration=[integer]`
  
  **Optional:**
  
     `description=[string]`


* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "name": "string",
        "description": "string",
        "price": "integer",
        "duration": "integer"
      },
      "code": { "code": 200 }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
        "error" : {
            "code": 400,
            "message": "The request is invalid"
        } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
        "error" : {
            "code": 401,
            "message": "Authentication is required"
        } 
    }
    ```
 
* **Sample Call:**

```bash
curl -X POST \
  http://localhost:8086/v1/plans \
  -H 'authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTI2NDU1NjIsImlhdCI6MTU1MjY0NDY2MiwiUGhvbmVOdW1iZXIiOiIiLCJVSUQiOjEsIlVzZXJJRCI6IkpPV3ZxRzNqYVg2OWxQN0R6YjhFIiwiUm9sZSI6IlJPTEVfQURNSU4ifQ.YvjClLTV6xy2T6gJG9ZSj4tazBV92HM-aHkyXes2dsg' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 10bf5278-de94-4889-c87a-1574d3d3f1e4' \
  -d '{
	"name":"plan2",
	"description":"description plan 2",
	"price": 800000,
	"duration": 8
}'
```

* **Notes:**
  
  
  
**List plan**
----
  List plan

* **URL**

  /api/v1/plans

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**

    `limit=[integer]`  A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.

    `offset=[integer]` The offset of the items returned. the default is 0

    `sortby=[string]`   Sorted by fields. Support sorted multiple fields, each field separate by comma. e.g: col1,col2

    `order=[string]`   Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc

    `query=[string]`   Filter. e.g. category:1,name:shop

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "data": [
        {
          "id": "string",
          "name": "string",
          "price": "number",
          "description": "string",
          "duration": "integer"
        },
        {...}
      ],
      "error": { "code": 200 } 
    }
    ```
 
* **Error Response:**

* **Notes:**


**Delete a plan**
----
  Delete a plan

* **URL**

  /api/v1/admin/plans/{plan_id}

* **Headers**

  `Authorization: Bearer <token>`

* **Method:**
  
  `DELETE`
  
*  **URL Params**

   None

* **Data Params**

  None

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** 
    ```json
    { 
      "data": {
        "id": "string",
        "deleted": "boolean"
      },
      "error": {
        "code": 200
      }
    }
    ```
 
* **Error Response:**

  * **Status Code:** 400 Bad request <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 400,
        "message": "The request is invalid"
      } 
    }
    ```
    
  * **Status Code:** 401 Unauthorized <br />
    **Content:** 
    ```json
    { 
      "error" : {
        "code": 401,
        "message": "Authentication is required"
      } 
    }
    ```
 
 
* **Notes:**


